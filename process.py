#!/usr/bin/env python3
"""Usage:
    process.py scan [--isbnf <file>]
    process.py convert [--isbnf <file>] [--bkcsv <file>]
    process.py shelve [--oldbks <file>] [--newbks <file>]
    process.py list [--bkcsv <file>] [--page <file>]
    process.py unmark [--bkcsv <file>]
    process.py --version
    process.py (-h | --help)

Options:
    -h --help  Show this screen.
    --isbnf    File with one ISBN number per line.
    --bkcsv    CSV file with Books.
    --oldbks   CSV file with previously shelved Books.
    --newbks   CSV file with new Books not yet shelved.
    --page     HTML file to generate list of shelved Books.
"""
import csv
import os
import sys

from docopt import docopt

import acclib


def scan_to_file(fp=open('data/isbn.dat', 'a')):
    new_isbns = acclib.scan_to_isbn([])

    for isbn in new_isbns:
        fp.write(f'{isbn}\n')

    fp.close()


def isbns_to_csv_books(isbnpath='data/isbn.dat', bookpath='data/newbooks.csv'):
    """Convert isbn list to books in a csv list"""
    f = open(isbnpath, 'r')
    isbns = [isbn.strip() for isbn in f.readlines()][2:]
    f.close()

    csvf = open(bookpath, 'a')
    bookwriter = csv.writer(
                     csvf,
                     delimiter=';',
                     quotechar='|',
                     quoting=csv.QUOTE_MINIMAL
                 )
    databases = acclib.connect_to_databases()

    for isbn in isbns:
        try:
            book = acclib.isbn_to_book(isbn, databases)
            bookwriter.writerow([
                acclib.format_book_title(book.title),
                book.callnum,
                book.isbn,
                book.status
            ])
            isbns.remove(isbn)
        except acclib.IncompleteData:
            pass

    acclib.disconnect_from_databases(databases)

    isbnf = open(isbnpath, 'w')
    isbnf.write('ISBN NUMBER\n')
    isbnf.write('===========\n')

    for isbn in isbns:
        isbnf.write(f'{isbn}\n')

    isbnf.close()


def insert_new_books(bf='data/books.csv', nbf='data/newbooks.csv'):
    with open(bf) as bookfile:
        bookreader = csv.reader(bookfile, delimiter=';', quotechar='|')
        books = acclib.read_books(bookreader)
    with open(nbf) as newbookfile:
        bookreader = csv.reader(newbookfile, delimiter=';', quotechar='|')
        newbooks = acclib.read_books(bookreader)

    while newbooks:
        books = acclib.insert_book_in_booklist(newbooks.pop(), books)

    acclib.write_books(books, bf)
    acclib.write_books(newbooks, nbf)


def mark_not_new(bf='data/books.csv'):
    with open(bf) as bookfile:
        bookreader = csv.reader(bookfile, delimiter=';', quotechar='|')
        books = acclib.read_books(bookreader)
        for book in books:
            book.status = 'N'

    acclib.write_books(books, bf)


def make_book_list_page():
    # Read in book page template
    f = open('views/books.tpl', 'r')
    page_top = f.read()
    f.close()

    # Read in books and iterate over them adding each as entry in books table
    with open('data/books.csv') as csv_books:
        book_reader = csv.reader(csv_books, delimiter=';', quotechar='|')
        books = acclib.read_books(book_reader)
        bookrows = ''
        for book in books:
            title = book.title
            callnum = book.callnum
            isbn = book.isbn
            open_tr = '<tr class="new">' if book.status == 'Y' else '<tr>'
            entry = f'{open_tr}\n<td>{title[:85]}</td>\n<td>{callnum}</td>\
                    \n<td>{isbn}</td>\n</tr>\n'
            bookrows += entry

    page_bottom = '</table>\n</main>\n</body>\n</html>'

    # Create book list webpage
    f = open('data/books.html', 'w')
    f.write(page_top + bookrows + page_bottom)
    f.close()


if __name__ == '__main__':
    arguments = docopt(__doc__, version='process.py 0.1')

    if arguments['scan']:
        if arguments['--isbnf']:
            fn = arguments['<file>'][0]
            try:
                f = open(fn, 'a')
                scan_to_file(f)
            except PermissionError:
                print(f"Permission error. Can not open: {fn}")
        else:
            scan_to_file()

    elif arguments['convert']:
        if arguments['--isbnf'] and arguments['--bkcsv']:
            isbnf = arguments['<file>'][0]
            bkf = arguments['<file>'][1]
        elif arguments['--isbnf']:
            isbnf = arguments['<file>'][0]
        elif arguments['--bkcsv']:
            bkf = arguments['<file>'][0]
        else:
            isbnf = 'data/isbn.dat'
            bkf = 'data/newbooks.csv'
        isbns_to_csv_books(isbnf, bkf)

    elif arguments['shelve']:
        if arguments['--oldbks'] and arguments['--newbks']:
            bf = arguments['<file>'][0]
            nbf = arguments['<file>'][1]
        elif arguments['--oldbks']:
            bf = arguments['<file>'][0]
        elif arguments['--bkcsv']:
            nbf = arguments['<file>'][0]
        else:
            bf = 'data/books.csv'
            nbf = 'data/newbooks.csv'
        insert_new_books()

    elif arguments['list']:
        make_book_list_page()

    elif arguments['unmark']:
        if arguments['--bkcsv']:
            bf = arguments['<file>'][0]
        else:
            bf = 'data/books.csv'
        mark_not_new(bf)
