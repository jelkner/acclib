# Data Flow

Processing books requires the following steps:

#. Scan the ISBN number
#. Lookup the title and Library of Congress (LC) call number using the ISBN.
#. Insert the book  into a list sorted by LC call number.
#. Add books to a webpage

The following files are associated with the above steps:

#. isbn.dat
#. newbooks.csv
#. books.csv
#. books.html
