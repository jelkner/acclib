DROP TABLE IF EXISTS Books;

CREATE TABLE Books (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT NOT NULL,
    class TEXT NOT NULL,
    subclass REAL NOT NULL,
    cutter TEXT, 
    suppl TEXT, 
    isbn TEXT,
    stat TEXT NOT NULL
);
