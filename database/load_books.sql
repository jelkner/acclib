INSERT INTO Books VALUES(
    1,
    'Good Faith Collaboration: The Culture of Wikipedia',
    'AE',
    100,
    'R43',
    '2010',
    '9780262014472',
    'Shelf'
);
INSERT INTO Books VALUES(
    2,
    'Pragmatism as Humanism: The Philosophy of William James',
    'B',
    945,
    'J24 D66',
    '',
    '0882291254',
    'Shelf'
);
INSERT INTO Books VALUES(
    3,
    'The Blue and Brown Books: Preliminary Studies for the "Philosophical Investigations"',
    'B',
    3376,
    'W563 B59',
    '1960',
    'N/A',
    'Shelf'
);
INSERT INTO Books VALUES(
    4,
    'Argument and Inference: An Introduction to Symbolic Logic',
    'BC',
    135,
    'C29',
    '',
    '0675083680',
    'Shelf'
);
INSERT INTO Books VALUES(
    5,
    'Algorithms to Live By: The Computer Science of Human Decisions',
    'BF',
    39,
    'C4885',
    '2016',
    '9781627790369',
    'Shelf'
);
INSERT INTO Books VALUES(
    6,
    'Next of Kin: What Chimpanzees Have Taught Me About Who We Are',
    'BF',
    109,
    'F66 A3',
    '1997',
    '9780688148621',
    'Shelf'
);
INSERT INTO Books VALUES(
    7,
    'The Talent Code: Greatness Isn''t Born. It''s Grown. Here''s How.',
    'BF',
    431,
    'C69',
    '2009',
    '9780553806847',
    'Shelf'
);
INSERT INTO Books VALUES(
    8,
    'Malcolm X: A Life of Reinvention',
    'BP',
    223,
    'Z8 L57636',
    '2011',
    '9780670022205',
    'Shelf'
);
INSERT INTO Books VALUES(
    9,
    'Hitler''s Willing Executioners: Ordinary Germans and the Holocaust',
    'D',
    804.3,
    'G648',
    '1996',
    '9780679446958',
    'Shelf'
);
INSERT INTO Books VALUES(
    10,
    'Thunder at Twilight: Vienna 1913/1914',
    'DB',
    855,
    'M67',
    '1989',
    '9780684191430',
    'Shelf'
);
INSERT INTO Books VALUES(
    11,
    'The Rape of Nanking: The Forgotten Holocaust of World War II',
    'DS',
    796,
    'N2 C44',
    '1997',
    '9780965604925',
    'Shelf'
);
INSERT INTO Books VALUES(
    12,
    'Long Walk to Freedom: The Autobiography of Nelson Mandela',
    'DT',
    1949,
    'M35 A3',
    '1994',
    '9780316545853',
    'Shelf'
);
INSERT INTO Books VALUES(
    13,
    'Mandela: The Authorized Biography',
    'DT',
    1974,
    'S26',
    '1999',
    '9780375400193',
    'Shelf'
);
INSERT INTO Books VALUES(
    14,
    'A Spirited Resistance: The North American Indian Struggle for Unity, 1745-1815',
    'E',
    77,
    'D694',
    '1992',
    '9780801846090',
    'shelf'
);
INSERT INTO Books VALUES(
    15,
    'Lies My Teacher Told Me: Everything Your American History Textbook Got Wrong',
    'E',
    175.85,
    'L64',
    '1995',
    '9781565841000',
    'shelf'
);
INSERT INTO Books VALUES(
    16,
    'The Color of Law: A Forgotten History of How Our Government Segregated America',
    'E',
    185.61,
    'R8185',
    '2017',
    '9781631492853',
    'shelf'
);
INSERT INTO Books VALUES(
    17,
    'Makes Me Wanna Holler: A Young Black Man in America',
    'E',
    185.97,
    'M12 A3',
    '1994',
    '9780679412687',
    'shelf'
);
INSERT INTO Books VALUES(
    18,
    'The Half Has Never Been Told: Slavery and the Making of American Capitalism',
    'E',
    441,
    'B337',
    '2014',
    '9780465044702',
    'shelf'
);
INSERT INTO Books VALUES(
    19,
    'Harriet Tubman: The Moses of Her People',
    'E',
    444,
    'T82 B73',
    '1993',
    '9781557092175',
    'shelf'
);
INSERT INTO Books VALUES(
    20,
    'My Bondage and My Freedom',
    'E',
    449,
    'D738',
    '1969',
    '9780486224572',
    'shelf'
);
INSERT INTO Books VALUES(
    21,
    'Gotham: A History of New York City to 1898',
    'F',
    128.3,
    'B87',
    '1999',
    '9780965068536',
    'shelf'
);
INSERT INTO Books VALUES(
    22,
    'Old Dominion, New Commonwealth: A History of Virginia, 1607-2007',
    'F',
    226,
    'O53',
    '2007',
    '9780813927695',
    'shelf'
);
INSERT INTO Books VALUES(
    23,
    'Gis Fundamentals: A First Text on Geographic Information Systems',
    'G',
    70.212,
    'B64',
    '2012',
    '9780971764736',
    'shelf'
);
INSERT INTO Books VALUES(
    24,
    'Manual of Geospatial Science and Technology',
    'G',
    70.212,
    'M287',
    '2010',
    '9781420087338',
    'shelf'
);
INSERT INTO Books VALUES(
    25,
    'PostGIS in Action',
    'G',
    70.212,
    'O23',
    '2015',
    '9781617291395',
    'shelf'
);
INSERT INTO Books VALUES(
    26,
    'Spatial Databases: A Tour',
    'G',
    70.212,
    'S54',
    '2003',
    '9780130174802',
    'shelf'
);
INSERT INTO Books VALUES(
    27,
    'Python Geospatial Development: Learn to Build Sophisticated Mapping Applications from Scratch Using Python Tools for Geospatial Development',
    'G',
    70.212,
    'W47',
    '2013',
    '9781782161523',
    'shelf'
);
INSERT INTO Books VALUES(
    28,
    'Learning Geospatial Analysis with Python',
    'G',
    70.217,
    'G46 L39',
    '2013',
    '9781783281138',
    'shelf'
);
INSERT INTO Books VALUES(
    29,
    'Elementary Statistics for Geographers',
    'G',
    70.3,
    'B37',
    '2009',
    '9781572304840',
    'shelf'
);
INSERT INTO Books VALUES(
    30,
    'How to Lie with Maps',
    'G',
    108.7,
    'M66',
    '1996',
    '9780226534213',
    'shelf'
);
INSERT INTO Books VALUES(
    31,
    'Human Geography: Landscapes of Human Activities 12th Edition',
    'GF',
    41,
    'H93',
    '2012',
    '9780078021466',
    'shelf'
);
INSERT INTO Books VALUES(
    32,
    'Human Geography',
    'GF',
    43,
    'M27',
    '2013',
    '9780470174685',
    'shelf'
);
INSERT INTO Books VALUES(
    33,
    'The Time Before History: 5 Million Years of Human Impact',
    'GN',
    281,
    'T84',
    '1996',
    '9780684807263',
    'shelf'
);
INSERT INTO Books VALUES(
    34,
    'The Wisdom of the Bones: In Search of Human Origins',
    'GN',
    284,
    'W35',
    '1996',
    '9780679426240',
    'shelf'
);
INSERT INTO Books VALUES(
    35,
    'Africa Counts: Number and Pattern in African Culture',
    'GN',
    476.15,
    'Z37',
    '1990',
    '9781556520754',
    'shelf'
);
INSERT INTO Books VALUES(
    36,
    'Introduction to Statistics',
    'HA',
    29,
    'H835',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    37,
    'The Wealth of Nations',
    'HB',
    161,
    'S65',
    '2003',
    '9780553585971',
    'shelf'
);
INSERT INTO Books VALUES(
    38,
    'The End of Growth Adapting to Our New Economic Reality',
    'HC',
    59.3,
    'H45',
    '2011eb',
    '9780865716957',
    'shelf'
);
INSERT INTO Books VALUES(
    39,
    'The Starfish and the Spider: The Unstoppable Power of Leaderless Organizations',
    'HD',
    50,
    'B73',
    '2006',
    '9781591841838',
    'shelf'
);
INSERT INTO Books VALUES(
    40,
    'Reinventing Organizations: A Guide to Creating Organizations Inspired by the Next Stage of Human Consciousness',
    'HD',
    58.8,
    'L356',
    '2014',
    '9782960133509',
    'shelf'
);
INSERT INTO Books VALUES(
    41,
    'The Open Organization: Igniting Passion and Performance',
    'HD',
    58.9,
    'W526',
    '2015',
    '9781625275271',
    'shelf'
);
INSERT INTO Books VALUES(
    42,
    'Life, Money, and Illusion: Living on Earth as if We Want to Stay',
    'HD',
    75,
    'N53',
    '2009',
    '9780865716599',
    'shelf'
);
INSERT INTO Books VALUES(
    43,
    'The Precariat: The New Dangerous Class',
    'HD',
    5857,
    'S73',
    '2014',
    '9781472536167',
    'shelf'
);
INSERT INTO Books VALUES(
    44,
    'Growing Up in Coal Country',
    'HD',
    6247,
    'M6152 U63',
    '1996',
    '9780395778470',
    'shelf'
);
INSERT INTO Books VALUES(
    45,
    'Who Built America?: Working People and the Nation''s Economy, Politics, Culture, and Society',
    'HD',
    8066,
    'W47',
    '2000',
    '9781572593039',
    'shelf'
);
INSERT INTO Books VALUES(
    46,
    'The World is Flat: A Brief History of the Twenty-First Century',
    'HM',
    846,
    'F74',
    '2005',
    '9780312425074',
    'shelf'
);
INSERT INTO Books VALUES(
    47,
    'Ours to Hack and to Own: The Rise of Platform Cooperativism, A New Vision for the Future of Work and a Fairer Internet',
    'HM',
    851,
    'O973',
    '2016',
    '9781944869335',
    'shelf'
);
INSERT INTO Books VALUES(
    48,
    'Culture Against Man',
    'HN',
    58,
    'H4',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    49,
    'City of Quartz: Excavating the Future in Los Angeles',
    'HN',
    80,
    'L7 D38',
    '1992',
    '9780679738060',
    'shelf'
);
INSERT INTO Books VALUES(
    50,
    'Dead End: Suburban Sprawl and the Rebirth of American Urbanism',
    'HT',
    352,
    'U6 R677',
    '2014',
    '9780199360147',
    'shelf'
);
INSERT INTO Books VALUES(
    51,
    'Suburban Nation: The Rise of Sprawl and the Decline of the American Dream',
    'HT',
    384,
    'U5 D83',
    '2000',
    '9780865475571',
    'shelf'
);
INSERT INTO Books VALUES(
    52,
    'Fire in the Ashes: Twenty-Five Years Among the Poorest Children in America',
    'HV',
    741,
    'K674',
    '2012b',
    '9781400052479',
    'shelf'
);
INSERT INTO Books VALUES(
    53,
    'Living a Happy Life with a Special-Needs Child: A Parent''s Perspective. Volume 1',
    'HV',
    888,
    'E92',
    '2017',
    '9780989908900',
    'shelf'
);
INSERT INTO Books VALUES(
    54,
    'Mathematics and Politics',
    'JA',
    73,
    'A4',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    55,
    'The Prince',
    'JC',
    143,
    'M3813',
    '1992',
    '9780486272740',
    'shelf'
);
INSERT INTO Books VALUES(
    56,
    'The Mathematics of Elections and Voting',
    'JF',
    1001,
    'W35',
    '2014',
    '9783319098098',
    'shelf'
);
INSERT INTO Books VALUES(
    57,
    'Beyond Transparency: Open Data and the Future of Civic Innovation',
    'JK',
    468,
    'T7 B49',
    '2013',
    '9780615889085',
    'shelf'
);
INSERT INTO Books VALUES(
    58,
    'Free Culture: How Big Media Uses Technology and the Law to Lock Down Culture and Control Creativity',
    'KF',
    2979,
    'L47',
    '2004',
    '9781594200465',
    'shelf'
);
INSERT INTO Books VALUES(
    59,
    'The American School 1642-1985',
    'LA',
    205,
    'S64',
    '1986',
    '0582285712',
    'shelf'
);
INSERT INTO Books VALUES(
    60,
    'Stories of the Eight-Year Study Reexamining Secondary Education in America',
    'LA',
    209,
    'K75',
    '2007',
    '9780791470534',
    'shelf'
);
INSERT INTO Books VALUES(
    61,
    'Narrowing the Achievement Gap: Perspectives and Strategies for Challenging Times',
    'LA',
    217.2,
    'N358',
    '2012',
    '9781612501239',
    'shelf'
);
INSERT INTO Books VALUES(
    62,
    'Taking Sides: Clashing Views on Controversial Educational Issues',
    'LA',
    217.2,
    'T35',
    '1989',
    '0879677597',
    'shelf'
);
INSERT INTO Books VALUES(
    63,
    'Doc: The Story of Dennis Littky and His Fight for a Better School',
    'LA',
    2317,
    'L68 K46',
    '2005',
    '9781416602286',
    'shelf'
);
INSERT INTO Books VALUES(
    64,
    'Democracy and Education: An Introduction to the Philosophy of Education',
    'LB',
    875,
    'D35',
    '2004',
    '9781449983673',
    'shelf'
);
INSERT INTO Books VALUES(
    65,
    'Pedagogy of Hope: Reliving Pedagogy of the Oppressed',
    'LB',
    880,
    'F732 P432',
    '1994',
    '9780826477903',
    'shelf'
);
INSERT INTO Books VALUES(
    66,
    'Freire for the Classroom: A Sourcebook for Liberatory Teaching',
    'LB',
    1025.2,
    'F73',
    '1987',
    '9780867091977',
    'shelf'
);
INSERT INTO Books VALUES(
    67,
    'Enhancing Professional Practice: A Framework for Teaching',
    'LB',
    1025.3,
    'D35',
    '2007',
    '9781416605171',
    'shelf'
);
INSERT INTO Books VALUES(
    68,
    'The Highly Engaged Classroom',
    'LB',
    1025.3,
    'M3425',
    '2011',
    '9780982259245',
    'shelf'
);
INSERT INTO Books VALUES(
    69,
    'Who Owns the Learning? Preparing Students for Success in the Digital Age',
    'LB',
    1027.23,
    'N68',
    '2012',
    '9781935542575',
    'shelf'
);
INSERT INTO Books VALUES(
    70,
    'Project Based Teaching: How to Create Rigorous and Engaging Learning Experiences',
    'LB',
    1027.43,
    'B65',
    '2018',
    '9781416626732',
    'shelf'
);
INSERT INTO Books VALUES(
    71,
    'High-Tech Heretic: Reflections of a Computer Contrarian',
    'LB',
    1028.5,
    'S77',
    '',
    '9780385489768',
    'shelf'
);
INSERT INTO Books VALUES(
    72,
    'The One World Schoolhouse: Education Reimagined',
    'LB',
    1044.87,
    'K485',
    '2012',
    '9781455508389',
    'shelf'
);
INSERT INTO Books VALUES(
    73,
    'Multiple Intelligences: The Theory in Practice',
    'LB',
    1060,
    'G357',
    '1993',
    '9780465018222',
    'shelf'
);
INSERT INTO Books VALUES(
    74,
    'How People Learn: Brain, Mind, Experience, and School',
    'LB',
    1060,
    'H672',
    '2000eb',
    '9780309070362',
    'shelf'
);
INSERT INTO Books VALUES(
    75,
    'I Won''t Learn from You: The Role of Assent in Learning',
    'LB',
    1060,
    'K65',
    '1991',
    '9780915943647',
    'shelf'
);
INSERT INTO Books VALUES(
    76,
    'Higher Expectations: Promoting Social Emotional Learning and Academic Achievement in Your School',
    'LB',
    1073,
    'P37',
    '2001',
    '9780807740910',
    'shelf'
);
INSERT INTO Books VALUES(
    77,
    'The Eight-Year Study Revisited: Lessons from the past for the Present',
    'LB',
    1623.5,
    'E54',
    '1998',
    '9781560901532',
    'shelf'
);
INSERT INTO Books VALUES(
    78,
    'The American Community College, 5th Edition',
    'LB',
    2323,
    'C55',
    '2008',
    '9780470174685',
    'shelf'
);
INSERT INTO Books VALUES(
    79,
    'Understanding Community Colleges',
    'LB',
    2328.15,
    'U6 U64',
    '2013',
    '9780415881272',
    'shelf'
);
INSERT INTO Books VALUES(
    80,
    'Critical Teaching and Everyday Life',
    'LB',
    2331,
    'S49',
    '1987',
    '0226753581',
    'shelf'
);
INSERT INTO Books VALUES(
    81,
    'Digital Diploma Mills: The Automation of Higher Education',
    'LB',
    2395.7,
    'N63',
    '2001',
    '1583670920',
    'shelf'
);
INSERT INTO Books VALUES(
    82,
    'Trusting Teachers with School Success: What Happens when Teachers Call the Shots',
    'LB',
    2806.45,
    'F36',
    '2013',
    '9781610485104',
    'shelf'
);
INSERT INTO Books VALUES(
    83,
    'Learning by Doing: A Handbook for Professional Learning Communities at Work',
    'LB',
    2822.82,
    'L427',
    '2010',
    '9781935542094',
    'shelf'
);
INSERT INTO Books VALUES(
    84,
    'A Repair Kit for Broken Grading: 15 Fixes for Broken Grades',
    'LB',
    3060.37,
    'O273',
    '2007',
    '9780132548687',
    'shelf'
);
INSERT INTO Books VALUES(
    85,
    'Pedagogy of the Heart',
    'LC',
    71,
    'F7413',
    '1997',
    '9780826411310',
    'shelf'
);
INSERT INTO Books VALUES(
    86,
    'Literacy: Reading the Word and the World',
    'LC',
    149,
    'F75',
    '1987',
    '9780897891264',
    'shelf'
);
INSERT INTO Books VALUES(
    87,
    'The Politics of Education: Culture, Power, and Liberation',
    'LC',
    149,
    'F76',
    '1985',
    '9780897890434',
    'shelf'
);
INSERT INTO Books VALUES(
    88,
    'American Education: An Introduction to Social and Political Aspects',
    'LC',
    191.4,
    'S684',
    '1985',
    '0582285445',
    'shelf'
);
INSERT INTO Books VALUES(
    89,
    'Education for Critical Consciousness',
    'LC',
    191.8,
    'L29 F74',
    '2005',
    '9780826477958',
    'shelf'
);
INSERT INTO Books VALUES(
    90,
    'Pedagogy of Freedom: Ethics, Democracy, and Civic Courage',
    'LC',
    196,
    'F73713',
    '1998',
    '9780847690473',
    'shelf'
);
INSERT INTO Books VALUES(
    91,
    'The Shame of the Nation: The Restoration of Apartheid Schooling in America',
    'LC',
    212.62,
    'K69',
    '2005',
    '9781400052455',
    'shelf'
);
INSERT INTO Books VALUES(
    92,
    'Warriors Don''t Cry: A Searing Memoir of the Battle to Integrate Little Rock''s Central High',
    'LC',
    214.23,
    'L56 B43',
    '1994',
    '9780671866396',
    'shelf'
);
INSERT INTO Books VALUES(
    93,
    'The Color of Their Skin: Education and Race in Richmond, Virginia, 1954-89',
    'LC',
    214.323,
    'R53 P63',
    '1992X',
    '0813913721',
    'shelf'
);
INSERT INTO Books VALUES(
    94,
    'Gaining on the Gap: Changing Hearts, Minds, and Practice',
    'LC',
    2717,
    'G35',
    '2011',
    '9781610482899',
    'shelf'
);
INSERT INTO Books VALUES(
    95,
    'Black Students-Middle Class Teachers',
    'LC',
    2731,
    'K86',
    '2002',
    '9780913543818',
    'shelf'
);
INSERT INTO Books VALUES(
    96,
    'Keeping Black Boys Out of Special Education',
    'LC',
    2731,
    'K865',
    '2005',
    '9780974900025',
    'shelf'
);
INSERT INTO Books VALUES(
    97,
    'Learning in a Burning House: Educational Inequality, Ideology, and (dis)integration',
    'LC',
    2741,
    'H67',
    '2011',
    '9780807751763',
    'shelf'
);
INSERT INTO Books VALUES(
    98,
    'Closing the Achievement Gap: A Vision for Changing Beliefs and Practices',
    'LC',
    4091,
    'C56',
    '2003',
    '9780871208385',
    'shelf'
);
INSERT INTO Books VALUES(
    99,
    'Reaching and Teaching Students in Poverty: Strategies for Erasing the Opportunity Gap',
    'LC',
    4091,
    'G595',
    '2013',
    '9780807754573',
    'shelf'
);
INSERT INTO Books VALUES(
    100,
    'Savage Inequalties: Children in America''s Schools',
    'LC',
    4091,
    'K69',
    '1991',
    '9780517582213',
    'shelf'
);
INSERT INTO Books VALUES(
    101,
    'Low Income Students and the Perpetuation of Inequality: Higher Education in America',
    'LC',
    4823,
    'B47',
    '2010',
    '9781409401544',
    'shelf'
);
INSERT INTO Books VALUES(
    102,
    'Larousse Encyclopedia of Music',
    'ML',
    160,
    'L34',
    '1971b',
    '0600023966',
    'shelf'
);
INSERT INTO Books VALUES(
    103,
    'Jazz: A History of America''s Music',
    'ML',
    3506,
    'W37',
    '2000',
    '067944551X',
    'shelf'
);
INSERT INTO Books VALUES(
    104,
    'The Cambridge Encyclopedia of Language',
    'P',
    29,
    'C64',
    '1987',
    '9780521424431',
    'shelf'
);
INSERT INTO Books VALUES(
    105,
    'Easy Spanish Step-By-Step: Mastering High-Frequency Grammar for Spanish Proficiency--Fast!',
    'PC',
    4112.5,
    'B74',
    '2006',
    '9780071463386',
    'shelf'
);
INSERT INTO Books VALUES(
    106,
    'Collins Spanish-English, English-Spanish Dictionary',
    'PC',
    4640,
    'S595',
    '',
    '9780060178024',
    'shelf'
);
INSERT INTO Books VALUES(
    107,
    'A Short History of English',
    'PE',
    1075,
    'G3',
    '1970',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    108,
    'English Grammar and Composition',
    'PE',
    1112,
    'W33',
    '1973',
    '0153119446',
    'shelf'
);
INSERT INTO Books VALUES(
    109,
    'Making a Difference: Personal Essays by Today''s College Teachers',
    'PE',
    1405,
    'U6 M33',
    '1998',
    '9780944210994',
    'shelf'
);
INSERT INTO Books VALUES(
    110,
    'My First Book of Chinese Words: An ABC Rhyming Book',
    'PL',
    1129,
    'E5 W723',
    '2012',
    '9780804843676',
    'shelf'
);
INSERT INTO Books VALUES(
    111,
    'Mandarin Chinese English Visual Bilingual Dictionary',
    'PL',
    1455,
    'M315',
    '2015',
    '9781465436337',
    'shelf'
);
INSERT INTO Books VALUES(
    112,
    'Quest for Chinese Culture',
    'PL',
    2928,
    'C63 X8613',
    '2010',
    '9787510407017',
    'shelf'
);
INSERT INTO Books VALUES(
    113,
    'Murphy''s Law and Other Reasons Why Things Go Wrong',
    'PN',
    6231,
    'M82 B57',
    '',
    '0843104287',
    'shelf'
);
INSERT INTO Books VALUES(
    114,
    'An Introduction to Spanish-American Literature',
    'PQ',
    7081,
    'F64',
    '1975',
    '0521098912',
    'shelf'
);
INSERT INTO Books VALUES(
    115,
    'The Works of Charles Dickens',
    'PR',
    4552,
    '',
    '1981',
    '0681270667',
    'shelf'
);
INSERT INTO Books VALUES(
    116,
    'The Annotated Alice: Alice''s Adventures in Wonderland & Through the Looking Glass',
    'PR',
    4611,
    'A7',
    '1960',
    '9780452010413',
    'shelf'
);
INSERT INTO Books VALUES(
    117,
    'Alice in Wonderland / Through the Looking-Glass and What Alice Found There',
    'PR',
    4611,
    'A7 P3',
    '1994',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    118,
    'A Portrait of the Artist as a Young Man',
    'PR',
    6019,
    'O9 P6484',
    '1956',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    119,
    'The Essential Whitman',
    'PS',
    3204,
    '',
    '1992',
    '0883658046',
    'shelf'
);
INSERT INTO Books VALUES(
    120,
    'The Education of Hyman Kaplan',
    'PS',
    3535,
    'O7577 E36',
    '1965',
    '9780156278119',
    'shelf'
);
INSERT INTO Books VALUES(
    121,
    '\"Harlem Gallery\", and Other Poems of Melvin B. Tolson',
    'PS',
    3539,
    'O334 A6',
    '1999',
    '9780813918655',
    'shelf'
);
INSERT INTO Books VALUES(
    122,
    'Wouldn''t Take Nothing for My Journey Now',
    'PS',
    3551,
    'N454 W6',
    '1993',
    '0394223632',
    'shelf'
);
INSERT INTO Books VALUES(
    123,
    'Even the Stars Look Lonesome',
    'PS',
    3551,
    'N464 E94',
    '1997b',
    '9780375500312',
    'shelf'
);
INSERT INTO Books VALUES(
    124,
    'Cryptonomicon',
    'PS',
    3569,
    'T3868 C79',
    '1999',
    '9780380788620',
    'shelf'
);
INSERT INTO Books VALUES(
    125,
    'Slapstick: Or, Lonesome No More!',
    'PS',
    3572,
    'O5 S58',
    '1976',
    '0440580099',
    'shelf'
);
INSERT INTO Books VALUES(
    126,
    'A Nest in Springtime: A Bilingual Book of Numbers',
    'PZ',
    10.831,
    'Y29 N47',
    '2012',
    '9780763652791',
    'shelf'
);
INSERT INTO Books VALUES(
    127,
    'Dictionary of Scientific Literacy',
    'Q',
    123,
    'B68',
    '1991',
    '0471532142',
    'shelf'
);
INSERT INTO Books VALUES(
    128,
    'Billions and Billions: Thoughts on Life and Death at the Brink of the Millennium',
    'Q',
    173,
    'S24',
    '1997',
    '9780679411604',
    'shelf'
);
INSERT INTO Books VALUES(
    129,
    'Prolog Programming for Artificial Intelligence',
    'Q',
    336,
    'B74',
    '2001',
    '9780201403756',
    'shelf'
);
INSERT INTO Books VALUES(
    130,
    'The Elements of Artificial Intelligence Using Common Lisp',
    'Q',
    336,
    'T37',
    '1990',
    '0716782693',
    'shelf'
);
INSERT INTO Books VALUES(
    131,
    'Learning and Teaching Geometry, K-12',
    'QA',
    1,
    'N3',
    '1987',
    '087353235X',
    'shelf'
);
INSERT INTO Books VALUES(
    132,
    'Rings and Ideals',
    'QA',
    3,
    'M31',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    133,
    'On Mathematics: A Collection of Witty, Profound, Amusing Passages about Mathematics and Mathematicians',
    'QA',
    3,
    'M7',
    '1942',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    134,
    'The World of Mathematics: A Small Library of the Literature of Mathematics from Aʻh-mosé the Scribe to Albert Einstein Presented with Commentaries and Notes',
    'QA',
    3,
    'N553',
    'v.1',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    135,
    'The World of Mathematics: A Small Library of the Literature of Mathematics from Aʻh-mosé the Scribe to Albert Einstein Presented with Commentaries and Notes',
    'QA',
    3,
    'N553',
    'v.2',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    136,
    'The World of Mathematics: A Small Library of the Literature of Mathematics from Aʻh-mosé the Scribe to Albert Einstein Presented with Commentaries and Notes',
    'QA',
    3,
    'N553',
    'v.3',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    137,
    'The World of Mathematics: A Small Library of the Literature of Mathematics from Aʻh-mosé the Scribe to Albert Einstein Presented with Commentaries and Notes',
    'QA',
    3,
    'N553',
    'v.4',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    138,
    'The Penguin Dictionary of Mathematics',
    'QA',
    5,
    'P4425',
    '1989',
    '9780140511192',
    'shelf'
);
INSERT INTO Books VALUES(
    139,
    'Popular Lectures on Mathematical Logic',
    'QA',
    9,
    'W347',
    '1993',
    '9780486676326',
    'shelf'
);
INSERT INTO Books VALUES(
    140,
    'Gödel, Escher, Bach: An Eternal Golden Braid',
    'QA',
    9.8,
    'H63',
    '1980',
    '0394745027',
    'shelf'
);
INSERT INTO Books VALUES(
    141,
    'How to Solve It: A New Aspect of Mathematical Method',
    'QA',
    11,
    'P6',
    '1957',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    142,
    'AGS Publishing Basic Math Skills',
    'QA',
    11,
    'T74',
    '2003',
    '9780785429524',
    'shelf'
);
INSERT INTO Books VALUES(
    143,
    'Everything You Need to Know About Math Homework',
    'QA',
    11,
    'Z46',
    '2005',
    '9780590493598',
    'shelf'
);
INSERT INTO Books VALUES(
    144,
    'Doing Math with Python: Use Programming to Explore Algebra, Statistics, Calculus, and More!',
    'QA',
    20,
    'C65 S24',
    '2015',
    '9781593276409',
    'shelf'
);
INSERT INTO Books VALUES(
    145,
    'Mathematics, Queen and Servant of Science',
    'QA',
    21,
    'B42',
    '1989',
    '9781556151736',
    'shelf'
);
INSERT INTO Books VALUES(
    146,
    'A History of Mathematics',
    'QA',
    21,
    'B767',
    '1991',
    '9780471543978',
    'shelf'
);
INSERT INTO Books VALUES(
    147,
    'Mathematical Thought from Ancient to Modern Times: Volume 1',
    'QA',
    21,
    'K516',
    '1990',
    '9780195061352',
    'shelf'
);
INSERT INTO Books VALUES(
    148,
    'Mathematical Thought from Ancient to Modern Times: Volume 2',
    'QA',
    21,
    'K516',
    '1990',
    '9780195061369',
    'shelf'
);
INSERT INTO Books VALUES(
    149,
    'Mathematical Thought from Ancient to Modern Times: Volume 3',
    'QA',
    21,
    'K516',
    '1990',
    '9780195061376',
    'shelf'
);
INSERT INTO Books VALUES(
    150,
    'A Concise History of Mathematics',
    'QA',
    21,
    'S87',
    '1987',
    '0486602559',
    'shelf'
);
INSERT INTO Books VALUES(
    151,
    'Rethinking Mathematics: Teaching Social Justice by the Numbers',
    'QA',
    22.3,
    'R478',
    '2013',
    '9780942961553',
    'shelf'
);
INSERT INTO Books VALUES(
    152,
    'Pi in the Sky: Counting, Thinking, and Being',
    'QA',
    36,
    'B37',
    '1992',
    '0198539568',
    'shelf'
);
INSERT INTO Books VALUES(
    153,
    'Mathematics: From the Birth of Numbers',
    'QA',
    36,
    'G86',
    '1997',
    '9780393040029',
    'shelf'
);
INSERT INTO Books VALUES(
    154,
    'Mathsemantics: Making Numbers Talk Sense',
    'QA',
    36,
    'M23',
    '1994',
    '0670853909',
    'shelf'
);
INSERT INTO Books VALUES(
    155,
    'Finite Mathematics',
    'QA',
    37.2,
    'L49',
    '1993',
    '9780673467270',
    'shelf'
);
INSERT INTO Books VALUES(
    156,
    'Mathematical Structures for Computer Science',
    'QA',
    39.2,
    'G47',
    '1993',
    '9780716782599',
    'shelf'
);
INSERT INTO Books VALUES(
    157,
    'Precalculus',
    'QA',
    39.2,
    'G66',
    '1994',
    '9780137164080',
    'shelf'
);
INSERT INTO Books VALUES(
    158,
    'Integrated Mathematics: Course III',
    'QA',
    39.2,
    'K442',
    '1982',
    '0877202524',
    'shelf'
);
INSERT INTO Books VALUES(
    159,
    'Precalculus',
    'QA',
    39.2,
    'L38',
    '1989',
    '9780669162776',
    'shelf'
);
INSERT INTO Books VALUES(
    160,
    'Precalculus',
    'QA',
    39.2,
    'M84',
    '1978',
    '087901086X',
    'shelf'
);
INSERT INTO Books VALUES(
    161,
    'Practical Mathematics, Sixth Edition',
    'QA',
    39.2,
    'P32',
    '1977',
    '0070482535',
    'shelf'
);
INSERT INTO Books VALUES(
    162,
    'Using Modern Mathematics',
    'QA',
    39.2,
    'S55',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    163,
    'Discrete Mathematics: A Unified Approach',
    'QA',
    39.2,
    'W53',
    '1987',
    '0070701695',
    'shelf'
);
INSERT INTO Books VALUES(
    164,
    'Finite Mathematics for the Managerial, Life, and Social Sciences',
    'QA',
    39.3,
    'T34',
    '2012',
    '9780840048141',
    'shelf'
);
INSERT INTO Books VALUES(
    165,
    'Charles Babbage on the Principles and Development of the Calculator',
    'QA',
    75,
    'C52',
    '1961',
    '0486246914',
    'shelf'
);
INSERT INTO Books VALUES(
    166,
    'Computer Science Illuminated',
    'QA',
    76,
    'D285',
    '2016',
    '9781284055917',
    'shelf'
);
INSERT INTO Books VALUES(
    167,
    'Beyond Calculation: The Next Fifty Years of Computing',
    'QA',
    76,
    'D348',
    '1997',
    '9780387985886',
    'shelf'
);
INSERT INTO Books VALUES(
    168,
    'The (new) Turing Omnibus: 66 Excursions in Computer Science',
    'QA',
    76,
    'D448',
    '1993',
    '9780716782711',
    'shelf'
);
INSERT INTO Books VALUES(
    169,
    'Computers - Theory and Uses: A Unit for Junior High School Classes',
    'QA',
    76,
    'N327',
    '1964',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    170,
    'Applied Computer Science',
    'QA',
    76,
    'T59',
    '2012',
    '9781461418870',
    'shelf'
);
INSERT INTO Books VALUES(
    171,
    'Computers, a Visual Encyclopedia',
    'QA',
    76.15,
    'K56',
    '1994',
    '9781567614640',
    'shelf'
);
INSERT INTO Books VALUES(
    172,
    'Scratch Programming for Teens',
    'QA',
    76.165,
    'F67',
    '2009',
    '9781598635362',
    'shelf'
);
INSERT INTO Books VALUES(
    173,
    'Free as in Freedom: Richard Stallman''s Crusade for Free Software',
    'QA',
    76.2,
    'S73 W55',
    '2002',
    '9780596002879',
    'shelf'
);
INSERT INTO Books VALUES(
    174,
    'Just for Fun: The Story of An Accidental Revolutionary',
    'QA',
    76.2,
    'T67 T67',
    '2001',
    '9780066620725',
    'shelf'
);
INSERT INTO Books VALUES(
    175,
    'Unlocking the Clubhouse: Women in Computing',
    'QA',
    76.25,
    'M35',
    '2001',
    '9780262632690',
    'shelf'
);
INSERT INTO Books VALUES(
    176,
    'Stuck in the Shallow End: Education, Race, and Computing',
    'QA',
    76.27,
    'M347',
    '2008eb',
    '9780262514040',
    'shelf'
);
INSERT INTO Books VALUES(
    177,
    'Java Software Structures for AP Computer Science AB',
    'QA',
    76.28,
    'L49',
    '2005',
    '9780321331618',
    'shelf'
);
INSERT INTO Books VALUES(
    178,
    'Comptia Security+ Exam Guide (exam Sy0-301): All in One',
    'QA',
    76.3,
    'C656564',
    '2011',
    '9780071771474',
    'shelf'
);
INSERT INTO Books VALUES(
    179,
    'The Handbook of Computers and Computing',
    'QA',
    76.5,
    'H3544',
    '1984',
    '0442231210',
    'shelf'
);
INSERT INTO Books VALUES(
    180,
    'Computers Simplified',
    'QA',
    76.5,
    'M2269',
    '1998',
    '9780764560422',
    'shelf'
);
INSERT INTO Books VALUES(
    181,
    'Linux Unleashing the Workstation in Your PC',
    'QA',
    76.5,
    'S78513',
    '1996',
    '9780387946016',
    'shelf'
);
INSERT INTO Books VALUES(
    182,
    'How Computers Work',
    'QA',
    76.5,
    'W488',
    '1993',
    '9781562760946',
    'shelf'
);
INSERT INTO Books VALUES(
    183,
    'Python for Kids: A Playful Introduction to Programming',
    'QA',
    76.52,
    'B75',
    '2013',
    '9781593274078',
    'shelf'
);
INSERT INTO Books VALUES(
    184,
    'Turtles, Termites, and Traffic Jams: Explorations in Massively Parallel Microworlds',
    'QA',
    76.58,
    'R47',
    '1994',
    '9780262680936',
    'shelf'
);
INSERT INTO Books VALUES(
    185,
    'The Design and Analysis of Computer Algorithms',
    'QA',
    76.6,
    'A36',
    '',
    '9780201000290',
    'shelf'
);
INSERT INTO Books VALUES(
    186,
    'Starting Out with Programming Logic and Design',
    'QA',
    76.6,
    'G315',
    '2013',
    '9780132805452',
    'shelf'
);
INSERT INTO Books VALUES(
    187,
    'Introduction to Algorithms',
    'QA',
    76.6,
    'I5858',
    '2001',
    '9780262032933',
    'shelf'
);
INSERT INTO Books VALUES(
    188,
    'The Unix Programming Environment',
    'QA',
    76.6,
    'K495',
    '1984',
    '013937681X',
    'shelf'
);
INSERT INTO Books VALUES(
    189,
    'The Art of Computer Programming: Volume 1 Fundamental Algorithms',
    'QA',
    76.6,
    'K64',
    '1997 v.1',
    '9780201896831',
    'shelf'
);
INSERT INTO Books VALUES(
    190,
    'The Art of Computer Programming: Volume 2 Seminumerical Algorithms',
    'QA',
    76.6,
    'K64',
    '1997 v.2',
    '9780201896848',
    'shelf'
);
INSERT INTO Books VALUES(
    191,
    'The Art of Computer Programming: Valume 3 Sorting and Searching',
    'QA',
    76.6,
    'K64',
    '1997 v.3',
    '9780201896855',
    'shelf'
);
INSERT INTO Books VALUES(
    192,
    'Programmers at Work: Interviews with 19 Programmers Who Shaped the Computer Industry',
    'QA',
    76.6,
    'L326',
    '1989',
    '9781556152115',
    'shelf'
);
INSERT INTO Books VALUES(
    193,
    'Karel the Robot: A Gentle Introduction to the Art of Programming',
    'QA',
    76.6,
    'P38',
    '',
    '9780471597254',
    'shelf'
);
INSERT INTO Books VALUES(
    194,
    'Code: The Hidden Language of Computer Hardware and Software',
    'QA',
    76.6,
    'P495',
    '1999',
    '9780735611313',
    'shelf'
);
INSERT INTO Books VALUES(
    195,
    'Structured Computer Organization',
    'QA',
    76.6,
    'T38',
    '1990',
    '9780138546625',
    'shelf'
);
INSERT INTO Books VALUES(
    196,
    'Introduction to Computing and Computer Science with Pascal',
    'QA',
    76.6,
    'W3275',
    '1985',
    '0316918415',
    'shelf'
);
INSERT INTO Books VALUES(
    197,
    'Designing Object-Oriented Software',
    'QA',
    76.6,
    'W5557',
    '1990',
    '9780136298250',
    'shelf'
);
INSERT INTO Books VALUES(
    198,
    'Objects First with Java: A Practical Introduction Using Bluej',
    'QA',
    76.64,
    'B385',
    '2017',
    '9780134477367',
    'shelf'
);
INSERT INTO Books VALUES(
    199,
    'Object-Oriented Analysis and Design with Applications',
    'QA',
    76.64,
    'B66',
    '1994',
    '9780805353402',
    'shelf'
);
INSERT INTO Books VALUES(
    200,
    'Object Solutions: Managing the Object-Oriented Project',
    'QA',
    76.64,
    'B67',
    '1996',
    '9780805305944',
    'shelf'
);
INSERT INTO Books VALUES(
    201,
    'Mastering JavaScript Object-Oriented Programming',
    'QA',
    76.64,
    'C45',
    '',
    '9781785889103',
    'shelf'
);
INSERT INTO Books VALUES(
    202,
    'Design Patterns: Elements of Reusable Object-oriented Software',
    'QA',
    76.64,
    'D47',
    '1995',
    '9780201633610',
    'shelf'
);
INSERT INTO Books VALUES(
    203,
    'Succeeding with Objects: Decision Frameworks for Project Management',
    'QA',
    76.64,
    'G63',
    '1995',
    '9780201628784',
    'shelf'
);
INSERT INTO Books VALUES(
    204,
    'Karel++: A Gentle Introduction to the Art of Object-Oriented Programming',
    'QA',
    76.64,
    'K37',
    '1997',
    '9780471138099',
    'shelf'
);
INSERT INTO Books VALUES(
    205,
    'Object-Oriented Software Metrics: A Practical Guide',
    'QA',
    76.64,
    'L67',
    '1994',
    '9780131792920',
    'shelf'
);
INSERT INTO Books VALUES(
    206,
    'Object-Oriented Development: The Fusion Method',
    'QA',
    76.64,
    'O247',
    '1994',
    '0133388239',
    'shelf'
);
INSERT INTO Books VALUES(
    207,
    'Truth, Deduction, and Computation: Logic and Semantics for Computer Science',
    'QA',
    76.7,
    'D386',
    '1989',
    '0716782014',
    'shelf'
);
INSERT INTO Books VALUES(
    208,
    'Concepts of Programming Languages',
    'QA',
    76.7,
    'S43',
    '2002',
    '9780201752953',
    'shelf'
);
INSERT INTO Books VALUES(
    209,
    'The C Trilogy: A Complete Library for C Programmers',
    'QA',
    76.73,
    'C15 B56',
    '1987',
    '0830628908',
    'shelf'
);
INSERT INTO Books VALUES(
    210,
    'A Book on C: Programming in C',
    'QA',
    76.73,
    'C15 K44',
    '1995',
    '9780805316773',
    'shelf'
);
INSERT INTO Books VALUES(
    211,
    'The C Programming Language',
    'QA',
    76.73,
    'C15 K47',
    '1988',
    '9780131103702',
    'shelf'
);
INSERT INTO Books VALUES(
    212,
    'The C Answer Book: Solutions to the Exercises in The C Programming Language, Second Edition, by Brian W. Kernighan and Dennis M. Ritchie',
    'QA',
    76.73,
    'C15 K47',
    '1988 suppl',
    '9780131096530',
    'shelf'
);
INSERT INTO Books VALUES(
    213,
    'C Programming: A Complete Guide to Mastering the C Language',
    'QA',
    76.73,
    'C15 H364',
    '1989',
    '9780201194449',
    'shelf'
);
INSERT INTO Books VALUES(
    214,
    'Fundamentals of Program Design and Data Structures with C++',
    'QA',
    76.73,
    'C15 L36',
    '1998',
    '031420492X',
    'shelf'
);
INSERT INTO Books VALUES(
    215,
    'Data Structures Using C',
    'QA',
    76.73,
    'C15 T46',
    '1990',
    '9780131997462',
    'shelf'
);
INSERT INTO Books VALUES(
    216,
    'C: A Reference Manual',
    'QA',
    76.73,
    'C15 H38',
    '1991',
    '9780131109339',
    'shelf'
);
INSERT INTO Books VALUES(
    217,
    'The Beginner''s Guide to C',
    'QA',
    76.73,
    'C15 H668',
    '1994',
    '9781874416159',
    'shelf'
);
INSERT INTO Books VALUES(
    218,
    'Fundamentals of C++: Understanding Programming and Problem Solving',
    'QA',
    76.73,
    'C15 L347',
    '1998',
    '0314204938',
    'shelf'
);
INSERT INTO Books VALUES(
    219,
    'C Primer Plus',
    'QA',
    76.73,
    'C15 P733',
    '1988',
    '0672225824',
    'shelf'
);
INSERT INTO Books VALUES(
    220,
    'C: The Complete Reference',
    'QA',
    76.73,
    'C15 S353',
    '1995',
    '9780078821011',
    'shelf'
);
INSERT INTO Books VALUES(
    221,
    'Easy Programming with C',
    'QA',
    76.73,
    'C15 T67',
    '1994',
    '9781565298880',
    'shelf'
);
INSERT INTO Books VALUES(
    222,
    'C++ Black Book',
    'QA',
    76.73,
    'C153 H645',
    '2001',
    '9781576107775',
    'shelf'
);
INSERT INTO Books VALUES(
    223,
    'Introduction to Computer Science Using C++',
    'QA',
    76.73,
    'C153 K64',
    '1998',
    '9780538676007',
    'shelf'
);
INSERT INTO Books VALUES(
    224,
    'Accelerated C++: Practical Programming by Example',
    'QA',
    76.73,
    'C153 K67',
    '2000',
    '9780201703535',
    'shelf'
);
INSERT INTO Books VALUES(
    225,
    'C++ the Core Language',
    'QA',
    76.73,
    'C153 S278',
    '1995eb',
    '9781565921160',
    'shelf'
);
INSERT INTO Books VALUES(
    226,
    'The C++ Programming Language',
    'QA',
    76.73,
    'C153 S77',
    '1997',
    '9780201889543',
    'shelf'
);
INSERT INTO Books VALUES(
    227,
    'Structured COBOL Programming',
    'QA',
    76.73,
    'C25 S75',
    '1988',
    '0471632872',
    'shelf'
);
INSERT INTO Books VALUES(
    228,
    'ANSI Common Lisp',
    'QA',
    76.73,
    'C28 G69',
    '1996',
    '9780133708752',
    'shelf'
);
INSERT INTO Books VALUES(
    229,
    'Erlang and OTP in Action',
    'QA',
    76.73,
    'E75 L64',
    '2011',
    '9781933988788',
    'shelf'
);
INSERT INTO Books VALUES(
    230,
    'Teach Yourself Programming in FORTRAN',
    'QA',
    76.73,
    'F25',
    '',
    '9780340275870',
    'shelf'
);
INSERT INTO Books VALUES(
    231,
    'Thinking in Java',
    'QA',
    76.73,
    'J38 E25',
    '1998',
    '9780136597230',
    'shelf'
);
INSERT INTO Books VALUES(
    232,
    'Data Structures and Algorithms in Java',
    'QA',
    76.73,
    'J38 D695',
    '2001',
    '9780534376680',
    'shelf'
);
INSERT INTO Books VALUES(
    233,
    'Java: How to Program',
    'QA',
    76.73,
    'J38 D45',
    '2003',
    '0131016210',
    'shelf'
);
INSERT INTO Books VALUES(
    234,
    'Thinking in Java',
    'QA',
    76.73,
    'J38 E25',
    '2003',
    '9780131002876',
    'shelf'
);
INSERT INTO Books VALUES(
    235,
    'Big Java',
    'QA',
    76.73,
    'J38 H674',
    '2002',
    '9780471402480',
    'shelf'
);
INSERT INTO Books VALUES(
    236,
    'Java Methods AB: Data Structures',
    'QA',
    76.73,
    'J38 L5848',
    '2003',
    '9780965485326',
    'shelf'
);
INSERT INTO Books VALUES(
    237,
    'Java Methods A & AB: Object-Oriented Programming and Data Structures',
    'QA',
    76.73,
    'J38 L588',
    '2006',
    '9780972705578',
    'shelf'
);
INSERT INTO Books VALUES(
    238,
    'Java Methods: Object-Oriented Programming and Data Structures',
    'QA',
    76.73,
    'J38 L656',
    '2011',
    '9780982477571',
    'shelf'
);
INSERT INTO Books VALUES(
    239,
    'Java Software Solutions for AP Computer Science',
    'QA',
    76.73,
    'J38 K4775',
    '2003',
    '0201882590',
    'shelf'
);
INSERT INTO Books VALUES(
    240,
    'Java Precisely',
    'QA',
    76.73,
    'J38 S435',
    '2002',
    '9780262692762',
    'shelf'
);
INSERT INTO Books VALUES(
    241,
    'Head First Java',
    'QA',
    76.73,
    'J38 S535',
    '2005',
    '9780596009205',
    'shelf'
);
INSERT INTO Books VALUES(
    242,
    'Mastering Javascript: Explore and Master Modern Javascript Techniques in Order to Build Large-Scale Web Applications / Ved Antani',
    'QA',
    76.73,
    'J39 A58',
    '2016',
    '9781785281341',
    'shelf'
);
INSERT INTO Books VALUES(
    243,
    'Eloquent JavaScript, 3rd Edition: A Modern Introduction to Programming',
    'QA',
    76.73,
    'J39 H38',
    '2019',
    '9781593279509',
    'shelf'
);
INSERT INTO Books VALUES(
    244,
    'Head First JavaScript Programming',
    'QA',
    76.73,
    'J39 F735',
    '2014',
    '9781449340131',
    'shelf'
);
INSERT INTO Books VALUES(
    245,
    'Javascript: The Good Parts',
    'QA',
    76.73,
    'J39 C763',
    '2008',
    '9780596517748',
    'shelf'
);
INSERT INTO Books VALUES(
    246,
    'DOM Scripting: Web Design with Javascript and the Document Object Model',
    'QA',
    76.73,
    'J39 K45',
    '2005',
    '9781590595336',
    'shelf'
);
INSERT INTO Books VALUES(
    247,
    'Javascript & Jquery',
    'QA',
    76.73,
    'J39 M388',
    '2011',
    '9781449399023',
    'shelf'
);
INSERT INTO Books VALUES(
    248,
    'Designing Pascal Solutions: A Case Study Approach',
    'QA',
    76.73,
    'P2 C525',
    '1992',
    '9780716782582',
    'shelf'
);
INSERT INTO Books VALUES(
    249,
    'Pascal Plus Data Structures',
    'QA',
    76.73,
    'P2 D35',
    '1991',
    '0669248304',
    'shelf'
);
INSERT INTO Books VALUES(
    250,
    'Computer Science with Pascal for Advanced Placement Students',
    'QA',
    76.73,
    'P2 M3',
    '1985',
    '0314896929',
    'shelf'
);
INSERT INTO Books VALUES(
    251,
    'PHP and MySQL for Dynamic Web Sites: Visual Quickpro Guide',
    'QA',
    76.73,
    'P224 U423',
    '2018',
    '9780134301846',
    'shelf'
);
INSERT INTO Books VALUES(
    252,
    'Learn to Program Using Python: A Tutorial for Hobbyists, Self-Starters, and All Who Want to Learn the Art of Computer Programming',
    'QA',
    76.73,
    'P48 G38',
    '2000',
    '9780201709384',
    'shelf'
);
INSERT INTO Books VALUES(
    253,
    'Python 3 Web Development Beginner''s Guide Use Python to Create, Theme, and Deploy Unique Web Applications',
    'QA',
    76.73,
    'P98 A53',
    '2011',
    '9781849513746',
    'shelf'
);
INSERT INTO Books VALUES(
    254,
    'Python Testing Beginner''s Guide: An Easy and Convenient Approach to Testing Your Python Projects',
    'QA',
    76.73,
    'P98 A73',
    '2010eb',
    '9781847198846',
    'shelf'
);
INSERT INTO Books VALUES(
    255,
    'Data Structures and Algorithms in Python',
    'QA',
    76.73,
    'P98 G66',
    '2013',
    '9788126562176',
    'shelf'
);
INSERT INTO Books VALUES(
    256,
    'Python Algorithms: Mastering Basic Algorithms in the Python Language',
    'QA',
    76.73,
    'P98 H485',
    '2010',
    '9781430232377',
    'shelf'
);
INSERT INTO Books VALUES(
    257,
    'Python Web Programming',
    'QA',
    76.73,
    'P98 H65',
    '2002',
    '9780735710900',
    'shelf'
);
INSERT INTO Books VALUES(
    258,
    'Test-Driven Development with Python: Obey the Testing Goat: Using Django, Selenium, and JavaScript',
    'QA',
    76.73,
    'P98 P46',
    '2017',
    '9781491958704',
    'shelf'
);
INSERT INTO Books VALUES(
    259,
    'Python Programming: An Introduction to Computer Science',
    'QA',
    76.73,
    'P98 Z45',
    '2004',
    '9781887902991',
    'shelf'
);
INSERT INTO Books VALUES(
    260,
    'Python for Data Analysis',
    'QA',
    76.73,
    'P98 M42',
    '2013',
    '9781449319793',
    'shelf'
);
INSERT INTO Books VALUES(
    261,
    'Teach Your Kids to Code: A Parent-Friendly Guide to Python Programming',
    'QA',
    76.73,
    'P98 P39',
    '2015',
    '9781593276140',
    'shelf'
);
INSERT INTO Books VALUES(
    262,
    'Introduction to Computer Science Using Python: A Computational Problem-Solving Focus',
    'QA',
    76.73,
    'P98 D547',
    '2013',
    '9780470555156',
    'shelf'
);
INSERT INTO Books VALUES(
    263,
    'Python: How to Program',
    'QA',
    76.73,
    'P98 P98',
    '2002',
    '9780130923615',
    'shelf'
);
INSERT INTO Books VALUES(
    264,
    'Think Python',
    'QA',
    76.73,
    'P98 D694',
    '2012',
    '9781449330729',
    'shelf'
);
INSERT INTO Books VALUES(
    265,
    'Problem Solving with Algorithms and Data Structures Using Python',
    'QA',
    76.73,
    'P98 M54',
    '2011',
    '9781590282571',
    'shelf'
);
INSERT INTO Books VALUES(
    266,
    'Expert Python Programming: Learn Best Practices to Designing, Coding, and Distributing Your Python Software',
    'QA',
    76.73,
    'P98 Z53',
    '2008',
    '9781847194947',
    'shelf'
);
INSERT INTO Books VALUES(
    267,
    'Python Testing Beginner''s Guide: An Easy and Convenient Approach to Testing Your Python Projects',
    'QA',
    76.73,
    'P98 A73',
    '2010',
    '9781847198846',
    'shelf'
);
INSERT INTO Books VALUES(
    268,
    'Mathematics for the Digital Age and Programming in Python',
    'QA',
    76.73,
    'P98 L58',
    '2010',
    '9780982477540',
    'shelf'
);
INSERT INTO Books VALUES(
    269,
    'Python in Education: Teach, Learn, Program',
    'QA',
    76.73,
    'P98',
    '',
    '9781491924624',
    'shelf'
);
INSERT INTO Books VALUES(
    270,
    'Programming the Raspberry Pi: Getting Started with Python',
    'QA',
    76.73,
    'P98 M66',
    '2013',
    '9780071807838',
    'shelf'
);
INSERT INTO Books VALUES(
    271,
    'Raspberry Pi Assembly Language: Beginners Hands On Guide',
    'QA',
    76.73,
    'P98 S55',
    '2013',
    '9781492135289',
    'shelf'
);
INSERT INTO Books VALUES(
    272,
    'Raspberry Pi Assembly Language: RISC OS Beginners Hands on Guide',
    'QA',
    76.73,
    'P98 S55',
    '2013',
    '9780992391621',
    'shelf'
);
INSERT INTO Books VALUES(
    273,
    'Programming in Python 3: A Complete Introduction to the Python Language',
    'QA',
    76.73,
    'P98 S86',
    '2009',
    '9780137129294',
    'shelf'
);
INSERT INTO Books VALUES(
    274,
    'Python Testing Cookbook over 70 Simple but Incredibly Effective Recipes for Taking Control of Automated Testing Using Powerful Python Testing Tools',
    'QA',
    76.73,
    'P98 T87',
    '2011',
    '9781849514668',
    'shelf'
);
INSERT INTO Books VALUES(
    275,
    'An Introduction to Python: Release 2.2.2',
    'QA',
    76.73,
    'P98 V36',
    '2003',
    '9780954161767',
    'shelf'
);
INSERT INTO Books VALUES(
    276,
    'The Scheme Programming Language: Ansi Scheme',
    'QA',
    76.73,
    'S23 D9',
    '1996',
    '9780134546469',
    'shelf'
);
INSERT INTO Books VALUES(
    277,
    'Scratch 1.4 Beginner''s Guide: Learn to Program While Creating Interactive Stories, Games, and Multimedia Projects Using Scratch',
    'QA',
    76.73,
    'S27 B34',
    '2009eb',
    '9781847196767',
    'shelf'
);
INSERT INTO Books VALUES(
    278,
    'The Little Schemer',
    'QA',
    76.73,
    'S34 F75',
    '1996',
    '9780262560993',
    'shelf'
);
INSERT INTO Books VALUES(
    279,
    'The Official Scratchjr Book: Help Your Kids Learn to Code!',
    'QA',
    76.73,
    'S345 B47',
    '2016',
    '9781593276713',
    'shelf'
);
INSERT INTO Books VALUES(
    280,
    'Scratch Programming in Easy Steps',
    'QA',
    76.73,
    'S345 M35',
    '2014',
    '9781840786125',
    'shelf'
);
INSERT INTO Books VALUES(
    281,
    'Learn to Program with Scratch: A Visual Introduction to Programming with Games, Art, Science, and Math',
    'QA',
    76.73,
    'S345 M38',
    '2014',
    '9781593275433',
    'shelf'
);
INSERT INTO Books VALUES(
    282,
    'The SQL Guide to SQLite',
    'QA',
    76.73,
    'S67 L3617',
    '2009',
    '9780557076765',
    'shelf'
);
INSERT INTO Books VALUES(
    283,
    'The Definitive Guide to Sqlite, Second Edition',
    'QA',
    76.73,
    'S67 A45',
    '2010eb',
    '9781430232254',
    'shelf'
);
INSERT INTO Books VALUES(
    284,
    'How Software Works',
    'QA',
    76.754,
    'W49',
    '1993',
    '9781562761332',
    'shelf'
);
INSERT INTO Books VALUES(
    285,
    'Free Software, Free Society: Selected Essays of Richard M. Stallman',
    'QA',
    76.756,
    'S73',
    '2002',
    '9781882114986',
    'shelf'
);
INSERT INTO Books VALUES(
    286,
    'Refactoring: Improving the Design of Existing Code',
    'QA',
    76.758,
    'F682',
    '2019',
    '9780134757599',
    'shelf'
);
INSERT INTO Books VALUES(
    287,
    'Working Effectively with Legacy Code',
    'QA',
    76.76,
    'A65 F43',
    '2005',
    '9780131177055',
    'shelf'
);
INSERT INTO Books VALUES(
    288,
    'Compiler Construction: Principles and Practice',
    'QA',
    76.76,
    'C65 L68',
    '1997',
    '9780534939724',
    'shelf'
);
INSERT INTO Books VALUES(
    289,
    'Writing Compilers and Interpreters: An Applied Approach',
    'QA',
    76.76,
    'C65 M35',
    '1991',
    '047150968X',
    'shelf'
);
INSERT INTO Books VALUES(
    290,
    'Build an HTML5 Game: A Developer''s Guide with CSS and JavaScript',
    'QA',
    76.76,
    'C672 B856',
    '2015',
    '9781593275754',
    'shelf'
);
INSERT INTO Books VALUES(
    291,
    'UML in a Nutshell: A Desktop Quick Reference',
    'QA',
    76.76,
    'D47 A424',
    '1998',
    '9781565924482',
    'shelf'
);
INSERT INTO Books VALUES(
    292,
    'Extreme Programming Explained: Embrace Change',
    'QA',
    76.76,
    'D47 B434',
    '2005',
    '9780321278654',
    'shelf'
);
INSERT INTO Books VALUES(
    293,
    'Head First Design Patterns',
    'QA',
    76.76,
    'D47 H427',
    '2004',
    '9780596007126',
    'shelf'
);
INSERT INTO Books VALUES(
    294,
    'Test-Driven Development: A Practical Guide',
    'QA',
    76.76,
    'D47 A783',
    '2003',
    '9780131016491',
    'shelf'
);
INSERT INTO Books VALUES(
    295,
    'Extreme Programming Applied: Playing to Win',
    'QA',
    76.76,
    'D47 A84',
    '2002',
    '9780201616408',
    'shelf'
);
INSERT INTO Books VALUES(
    296,
    'Practical Guide to Extreme Programming',
    'QA',
    76.76,
    'D47 A88',
    '2002',
    '9780130674821',
    'shelf'
);
INSERT INTO Books VALUES(
    297,
    'The Unified Modeling Language User Guide',
    'QA',
    76.76,
    'D47 B655',
    '1999',
    '9780201571684',
    'shelf'
);
INSERT INTO Books VALUES(
    298,
    'Clean Code: A Handbook of Agile Software Craftsmanship',
    'QA',
    76.76,
    'D47 C583',
    '2009',
    '9780132350884',
    'shelf'
);
INSERT INTO Books VALUES(
    299,
    'Testing Extreme Programming',
    'QA',
    76.76,
    'D47 C75',
    '2003',
    '9780321113559',
    'shelf'
);
INSERT INTO Books VALUES(
    300,
    'The Unified Software Development Process',
    'QA',
    76.76,
    'D47 J35',
    '1999',
    '9780201571691',
    'shelf'
);
INSERT INTO Books VALUES(
    301,
    'Extreme Programming Installed',
    'QA',
    76.76,
    'D47 J44',
    '2000',
    '9780201708424',
    'shelf'
);
INSERT INTO Books VALUES(
    302,
    'Object-Oriented Design Made Easy!: The Complete Guide to Several Popular Object-Oriented Design Techniques',
    'QA',
    76.76,
    'D47 M56',
    '1993',
    '0963663763',
    'shelf'
);
INSERT INTO Books VALUES(
    303,
    'Object-Oriented Software Engineering: A Use Case Driven Approach',
    'QA',
    76.76,
    'D47 O24',
    '1992',
    '9780201544350',
    'shelf'
);
INSERT INTO Books VALUES(
    304,
    'The Unified Modeling Language Reference Manual',
    'QA',
    76.76,
    'D47 R86',
    '1999',
    '9780201309980',
    'shelf'
);
INSERT INTO Books VALUES(
    305,
    'HTML5 for Web Designers',
    'QA',
    76.76,
    'H94 K45',
    '2010',
    '9780984442508',
    'shelf'
);
INSERT INTO Books VALUES(
    306,
    'HTML5: The Missing Manual',
    'QA',
    76.76,
    'H94 M33',
    '2014eb',
    '9781449363260',
    'shelf'
);
INSERT INTO Books VALUES(
    307,
    'New Perspectives on HTML5 and CSS3: Comprehensive',
    'QA',
    76.76,
    'H94 C36',
    '2017',
    '9781305503939',
    'shelf'
);
INSERT INTO Books VALUES(
    308,
    'Inside Linux: A Look at Operating System Development',
    'QA',
    76.76,
    'O63 B464',
    '1996',
    '9780916151898',
    'shelf'
);
INSERT INTO Books VALUES(
    309,
    'Networking Unix',
    'QA',
    76.76,
    'O63 D685',
    '1995',
    '9780672305849',
    'shelf'
);
INSERT INTO Books VALUES(
    310,
    'The Unix Philosophy',
    'QA',
    76.76,
    'O63 G365',
    '1995',
    '9781555581237',
    'shelf'
);
INSERT INTO Books VALUES(
    311,
    'Unix System Programming',
    'QA',
    76.76,
    'O63 H38',
    '1987',
    '9780201129199',
    'shelf'
);
INSERT INTO Books VALUES(
    312,
    'Linux Desk Reference',
    'QA',
    76.76,
    'O63 H386',
    '2002',
    '9780130619891',
    'shelf'
);
INSERT INTO Books VALUES(
    313,
    'Linux Companion for System Administrators / Jochen Hein',
    'QA',
    76.76,
    'O63 H4523',
    '1999',
    '9780201360448',
    'shelf'
);
INSERT INTO Books VALUES(
    314,
    'Linux Application Development',
    'QA',
    76.76,
    'O63 J635',
    '1998',
    '9780201308211',
    'shelf'
);
INSERT INTO Books VALUES(
    315,
    'Learning the Vi Editor',
    'QA',
    76.76,
    'O63 L355',
    '1990',
    '9780937175675',
    'shelf'
);
INSERT INTO Books VALUES(
    316,
    'The Linux Cookbook: Tips and Techniques for Everyday Use',
    'QA',
    76.76,
    'O63 S788',
    '2001',
    '9781886411487',
    'shelf'
);
INSERT INTO Books VALUES(
    317,
    'Linux: Rute User''s Tutorial and Exposition',
    'QA',
    76.76,
    'O63 S5527',
    '2002',
    '9780130333513',
    'shelf'
);
INSERT INTO Books VALUES(
    318,
    'The UNIX-Haters Handbook',
    'QA',
    76.76,
    'O63 U54518',
    '1994',
    '9781568842035',
    'shelf'
);
INSERT INTO Books VALUES(
    319,
    'Beginning Linux Programming',
    'QA',
    76.76,
    'O63 M37157',
    '1999',
    '9781861002976',
    'shelf'
);
INSERT INTO Books VALUES(
    320,
    'The Linux Bible: The GNU Testament',
    'QA',
    76.76,
    'O63 L5458',
    '1995',
    '9781883601126',
    'shelf'
);
INSERT INTO Books VALUES(
    321,
    'Unix System Administration Handbook',
    'QA',
    76.76,
    'O63 N45',
    '1989',
    '0139334416',
    'shelf'
);
INSERT INTO Books VALUES(
    322,
    'The Cathedral and the Bazaar: Musings on Linux and Open Source by An Accidental Revolutionary',
    'QA',
    76.76,
    'O63 R397',
    '2001',
    '9780596001087',
    'shelf'
);
INSERT INTO Books VALUES(
    323,
    'Modern Operating Systems',
    'QA',
    76.76,
    'O63 T359',
    '1992',
    '9780135881873',
    'shelf'
);
INSERT INTO Books VALUES(
    324,
    'Operating Systems: Design and Implementation',
    'QA',
    76.76,
    'O63 T36',
    '1986',
    '0136374069',
    'shelf'
);
INSERT INTO Books VALUES(
    325,
    'Using Linux',
    'QA',
    76.76,
    'O63 U718',
    '1998',
    '9780789716231',
    'shelf'
);
INSERT INTO Books VALUES(
    326,
    'Test-Driven Development: By Example',
    'QA',
    76.76,
    'T48 B43',
    '2003',
    '9780321146533',
    'shelf'
);
INSERT INTO Books VALUES(
    327,
    'Implementing Automated Software Testing: How to Save Time and Lower Costs While Raising Quality',
    'QA',
    76.76,
    'T48 D8744',
    '2009',
    '9780321580511',
    'shelf'
);
INSERT INTO Books VALUES(
    328,
    'GNU Emacs Pocket Reference',
    'QA',
    76.76,
    'T49 C34',
    '1999',
    '9781565924963',
    'shelf'
);
INSERT INTO Books VALUES(
    329,
    'Learning GNU Emacs',
    'QA',
    76.76,
    'T49 C35',
    '1996',
    '9781565921528',
    'shelf'
);
INSERT INTO Books VALUES(
    330,
    'Assembly Language for x86 Processors: Sixth Edition',
    'QA',
    76.8,
    'I77',
    '2011',
    '9780136022121',
    'shelf'
);
INSERT INTO Books VALUES(
    331,
    'Exploring Raspberry Pi: Interfacing to the Real World with Embedded Linux',
    'QA',
    76.8,
    'R15',
    '',
    '9781119188681',
    'shelf'
);
INSERT INTO Books VALUES(
    332,
    'Hello Raspberry Pi!: Python Programming for Kids and Other Beginners',
    'QA',
    76.8,
    'R15 H45',
    '2016',
    '9781617292453',
    'shelf'
);
INSERT INTO Books VALUES(
    333,
    'Raspberry Pi User Guide',
    'QA',
    76.8,
    'R19 U68',
    '2012',
    '9781118464465',
    'shelf'
);
INSERT INTO Books VALUES(
    334,
    'Data Structure and Algorithmic Thinking with Python',
    'QA',
    76.9,
    'A43 K368',
    '2016',
    '9788192107592',
    'shelf'
);
INSERT INTO Books VALUES(
    335,
    'Linux Server Hacks',
    'QA',
    76.9,
    'C55 F58',
    '2003',
    '9780596004613',
    'shelf'
);
INSERT INTO Books VALUES(
    336,
    'The Essentials of Computer Organization and Architecture',
    'QA',
    76.9,
    'C643 N85',
    '2015',
    '9781284074482',
    'shelf'
);
INSERT INTO Books VALUES(
    337,
    'Computer Organization and Design: The Hardware/software Interface',
    'QA',
    76.9,
    'C643 P37',
    '2009',
    '9780123744937',
    'shelf'
);
INSERT INTO Books VALUES(
    338,
    'The Linux Database',
    'QA',
    76.9,
    'D3 B893',
    '1997',
    '9781558284913',
    'shelf'
);
INSERT INTO Books VALUES(
    339,
    'The Database Relational Model: A Retrospective Review and Analysis: A Historical Account and Assessment of E.f. Codd''s Contribution to the Field of Database Technology',
    'QA',
    76.9,
    'D3 D368',
    '2001',
    '9780201612943',
    'shelf'
);
INSERT INTO Books VALUES(
    340,
    'Object Persistence: Beyond Object-Oriented Databases',
    'QA',
    76.9,
    'D3 S473',
    '1996',
    '9780131924369',
    'shelf'
);
INSERT INTO Books VALUES(
    341,
    'Database System Concepts',
    'QA',
    76.9,
    'D3 S5637',
    '2011',
    '9780073523323',
    'shelf'
);
INSERT INTO Books VALUES(
    342,
    'The Manga Guide to Databases',
    'QA',
    76.9,
    'D3 T34',
    '2009',
    '9781593271909',
    'shelf'
);
INSERT INTO Books VALUES(
    343,
    'Data Structures and Abstractions with Java',
    'QA',
    76.9,
    'D33 C37',
    '2007',
    '9780132370455',
    'shelf'
);
INSERT INTO Books VALUES(
    344,
    'Data Structures and Algorithms with Python',
    'QA',
    76.9,
    'D35',
    '',
    '9783319130712',
    'shelf'
);
INSERT INTO Books VALUES(
    345,
    'Data Structures and Algorithms',
    'QA',
    76.9,
    'D35 A38',
    '1982',
    '0201000237',
    'shelf'
);
INSERT INTO Books VALUES(
    346,
    'Discrete Mathematics',
    'QA',
    76.9,
    'M35 R67',
    '1988',
    '0132154277',
    'shelf'
);
INSERT INTO Books VALUES(
    347,
    'Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design',
    'QA',
    76.9,
    'O35 L37',
    '1998',
    '9780137488803',
    'shelf'
);
INSERT INTO Books VALUES(
    348,
    'The Art of Objects: Object-Oriented Design and Architecture',
    'QA',
    76.9,
    'O35 L39',
    '2001',
    '9780201711615',
    'shelf'
);
INSERT INTO Books VALUES(
    349,
    'Distributed Object-Oriented Data-Systems Design',
    'QA',
    76.9,
    'S88 A528',
    '1992',
    '9780131749139',
    'shelf'
);
INSERT INTO Books VALUES(
    350,
    'Sage Beginner''s Guide: Unlock the Full Potential of Sage for Simplifying and Automating Mathematical Computing',
    'QA',
    76.95,
    'F56',
    '2011',
    '9781849514460',
    'shelf'
);
INSERT INTO Books VALUES(
    351,
    'Mathematics and the Imagination',
    'QA',
    93,
    'K3',
    '1989',
    '9781556151040',
    'shelf'
);
INSERT INTO Books VALUES(
    352,
    'Islands of Truth: A Mathematical Mystery Cruise',
    'QA',
    93,
    'P474',
    '1990',
    '9780716721130',
    'shelf'
);
INSERT INTO Books VALUES(
    353,
    'The Mathematical Tourist: Snapshots of Modern Mathematics',
    'QA',
    93,
    'P475',
    '1988',
    '0716720647',
    'shelf'
);
INSERT INTO Books VALUES(
    354,
    'Enjoyment of Mathematics',
    'QA',
    93,
    'R3',
    '',
    '0691023514',
    'shelf'
);
INSERT INTO Books VALUES(
    355,
    'Mathemagics: How to Look Like a Genius without Really Trying',
    'QA',
    95,
    'B444',
    '1993',
    '0929923545',
    'shelf'
);
INSERT INTO Books VALUES(
    356,
    'Pre-Algebra Demystified',
    'QA',
    107.2,
    'B59',
    '2004',
    '9780071439312',
    'shelf'
);
INSERT INTO Books VALUES(
    357,
    'Mathematics with Numbers in Color: Book A',
    'QA',
    135,
    'G32',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    358,
    'Invitation to Mathematics: Grade 8',
    'QA',
    135.5,
    'I55',
    '1987',
    '0673235084',
    'shelf'
);
INSERT INTO Books VALUES(
    359,
    'Number Sense: Whole Numbers: Addition and Subtraction',
    'QA',
    139,
    'S953 v01',
    '',
    '9780072871043',
    'shelf'
);
INSERT INTO Books VALUES(
    360,
    'Number Sense: Whole Numbers: Multiplication and Division',
    'QA',
    139,
    'S953 v02',
    '',
    '9780072871050',
    'shelf'
);
INSERT INTO Books VALUES(
    361,
    'Number Sense: Decimals: Addition and Subtraction',
    'QA',
    139,
    'S953 v03',
    '',
    '9780072871067',
    'shelf'
);
INSERT INTO Books VALUES(
    362,
    'Number Sense: Decimals: Multiplication and Division',
    'QA',
    139,
    'S953 v04',
    '',
    '9780072871074',
    'shelf'
);
INSERT INTO Books VALUES(
    363,
    'Number Sense: Fractions: The Meaning of Fractions',
    'QA',
    139,
    'S953 v05',
    '',
    '9780072871081',
    'shelf'
);
INSERT INTO Books VALUES(
    364,
    'Number Sense: Fractions: Addition and Subtraction',
    'QA',
    139,
    'S953 v06',
    '',
    '9780072871098',
    'shelf'
);
INSERT INTO Books VALUES(
    365,
    'Number Sense: Fractions: Multiplication and Division',
    'QA',
    139,
    'S953 v07',
    '',
    '9780072871104',
    'shelf'
);
INSERT INTO Books VALUES(
    366,
    'Number Sense: Ration and Proportion',
    'QA',
    139,
    'S953 v08',
    '',
    '9780072871111',
    'shelf'
);
INSERT INTO Books VALUES(
    367,
    'Number Sense: The Meaning of Percent',
    'QA',
    139,
    'S953 v09',
    '',
    '9780072871128',
    'shelf'
);
INSERT INTO Books VALUES(
    368,
    'Number Sense: Percent Applications',
    'QA',
    139,
    'S953 v10',
    '',
    '9780072871135',
    'shelf'
);
INSERT INTO Books VALUES(
    369,
    'Numbers: Their History and Meaning',
    'QA',
    141.2,
    'F56',
    '1983',
    '1566191718',
    'shelf'
);
INSERT INTO Books VALUES(
    370,
    'Number Words and Number Symbols: A Cultural History of Numbers',
    'QA',
    141.2,
    'M4513',
    '1992',
    '9780486270968',
    'shelf'
);
INSERT INTO Books VALUES(
    371,
    'Concepts in Algebra: A Technological Approach',
    'QA',
    152.2,
    'C66',
    '1995',
    '9780939765737',
    'shelf'
);
INSERT INTO Books VALUES(
    372,
    'Gateways to Algebra and Geometry: An Integrated Approach',
    'QA',
    152.2,
    'G37',
    '1993',
    '0812376455',
    'shelf'
);
INSERT INTO Books VALUES(
    373,
    'Elementary Algebra for College Students',
    'QA',
    152.2,
    'K38',
    '1989',
    '9780534915735',
    'shelf'
);
INSERT INTO Books VALUES(
    374,
    'Painless Algebra',
    'QA',
    152.2,
    'L66',
    '1998',
    '9780764106767',
    'shelf'
);
INSERT INTO Books VALUES(
    375,
    'Merrill Algebra 2 with Trigonometry: Applications and Connections',
    'QA',
    152.2,
    'M47',
    '1992',
    '9780028242286',
    'shelf'
);
INSERT INTO Books VALUES(
    376,
    'Algebra 1',
    'QA',
    152.3,
    'A45',
    '1993',
    '9780669267501',
    'shelf'
);
INSERT INTO Books VALUES(
    377,
    'Algebra 2 and Trigonometry',
    'QA',
    154,
    '',
    '.s.A35 1983',
    '0395279267',
    'shelf'
);
INSERT INTO Books VALUES(
    378,
    'Advanced Algebra and Calculus Made Simple',
    'QA',
    154,
    'G57',
    '1959',
    '0385004389',
    'shelf'
);
INSERT INTO Books VALUES(
    379,
    'Intermediate Algebra and Analytic Geometry Made Simple',
    'QA',
    154,
    'G6',
    '',
    '0385004370',
    'shelf'
);
INSERT INTO Books VALUES(
    380,
    'Introductory Algebra',
    'QA',
    154,
    'W872',
    '',
    '067508511X',
    'shelf'
);
INSERT INTO Books VALUES(
    381,
    'College Algebra with Applications',
    'QA',
    154.2,
    'B385',
    '1985',
    '0314852174',
    'shelf'
);
INSERT INTO Books VALUES(
    382,
    'Fundamental Concepts of Algebra',
    'QA',
    154.2,
    'M47',
    '1982',
    '9780486614700',
    'shelf'
);
INSERT INTO Books VALUES(
    383,
    'Fundamental Concepts of Abstract Algebra',
    'QA',
    162,
    'E34',
    '1991',
    '0534924557',
    'shelf'
);
INSERT INTO Books VALUES(
    384,
    'Contemporary Abstract Algebra',
    'QA',
    162,
    'G34',
    '1990',
    '9780669194968',
    'shelf'
);
INSERT INTO Books VALUES(
    385,
    'Introduction to Graph Theory',
    'QA',
    166,
    'W55',
    '1985',
    '9780582446854',
    'shelf'
);
INSERT INTO Books VALUES(
    386,
    'Linear Algebra and Its Applications',
    'QA',
    184,
    'S8',
    '1988',
    '0155510053',
    'shelf'
);
INSERT INTO Books VALUES(
    387,
    'Elements of Number Theory: Including an Introduction to Equations over Finite Fields',
    'QA',
    241,
    'I67',
    '',
    '0800500253',
    'shelf'
);
INSERT INTO Books VALUES(
    388,
    'Foundations of Analysis: The Arithmetic of Whole, Rational, Irrational, and Complex Numbers',
    'QA',
    241,
    'L253',
    '1966',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    389,
    'Number Theory and Its History',
    'QA',
    241,
    'O7',
    '1988',
    '0486656209',
    'shelf'
);
INSERT INTO Books VALUES(
    390,
    'Fermat''s Last Theorem: Unlocking the Secret of an Ancient Mathematical Problem',
    'QA',
    244,
    'A29',
    '1996',
    '9781568580777',
    'shelf'
);
INSERT INTO Books VALUES(
    391,
    'Some Prime Comparisons',
    'QA',
    246,
    'B76',
    '',
    '0873531310',
    'shelf'
);
INSERT INTO Books VALUES(
    392,
    'On Formally Undecidable Propositions of Principia Mathematica and Related Systems',
    'QA',
    248,
    'G573',
    '1992',
    '9780486669809',
    'shelf'
);
INSERT INTO Books VALUES(
    393,
    'Axiomatic Set Theory',
    'QA',
    248,
    'S92',
    '1972',
    '9780486616308',
    'shelf'
);
INSERT INTO Books VALUES(
    394,
    'What Is Linear Programming?',
    'QA',
    265,
    'B3915',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    395,
    'A Concrete Approach to Abstract Algebra',
    'QA',
    266,
    'S28',
    '',
    '9780486636474',
    'shelf'
);
INSERT INTO Books VALUES(
    396,
    'Elements of the Theory of Computation',
    'QA',
    267,
    'L49',
    '1998',
    '9780132624787',
    'shelf'
);
INSERT INTO Books VALUES(
    397,
    'Introduction to Formal Languages',
    'QA',
    267.3,
    'R485',
    '1991',
    '9780486666976',
    'shelf'
);
INSERT INTO Books VALUES(
    398,
    'Probability: An Introduction',
    'QA',
    273,
    'G62',
    '',
    '0486652521',
    'shelf'
);
INSERT INTO Books VALUES(
    399,
    'Concepts of Probability Theory',
    'QA',
    273,
    'P4',
    '',
    '0486636771',
    'shelf'
);
INSERT INTO Books VALUES(
    400,
    'A First Course in Probability',
    'QA',
    273,
    'R83',
    '1988',
    '0024038504',
    'shelf'
);
INSERT INTO Books VALUES(
    401,
    'Introduction to Probability and Statistics',
    'QA',
    276,
    'M425',
    '1983',
    '0871503654',
    'shelf'
);
INSERT INTO Books VALUES(
    402,
    'Naked Statistics: Stripping the Dread from the Data',
    'QA',
    276,
    'W458',
    '2013',
    '9780393347777',
    'shelf'
);
INSERT INTO Books VALUES(
    403,
    'Think Stats',
    'QA',
    276.4,
    'D69',
    '2014',
    '9781491907337',
    'shelf'
);
INSERT INTO Books VALUES(
    404,
    'Think Bayes',
    'QA',
    279.5,
    'D69',
    '2013',
    '9781449370787',
    'shelf'
);
INSERT INTO Books VALUES(
    405,
    'Uses of Infinity',
    'QA',
    295,
    'Z5',
    '1962',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    406,
    'Elementary Numerical Analysis: An Algorithmic Approach',
    'QA',
    297,
    'C65',
    '',
    '0070124477',
    'shelf'
);
INSERT INTO Books VALUES(
    407,
    'Numerical Analysis: Mathematics of Scientific Computing',
    'QA',
    297,
    'K563',
    '1991',
    '9780534130145',
    'shelf'
);
INSERT INTO Books VALUES(
    408,
    'Introduction to Real Analysis',
    'QA',
    300,
    'B294',
    '',
    '0471059447',
    'shelf'
);
INSERT INTO Books VALUES(
    409,
    'Introductory Analysis',
    'QA',
    300,
    'I58',
    '1991',
    '0395524326',
    'shelf'
);
INSERT INTO Books VALUES(
    410,
    'Precalculus: Fundamentals of Mathematical Analysis',
    'QA',
    300,
    'L64',
    '',
    '0393093786',
    'shelf'
);
INSERT INTO Books VALUES(
    411,
    'Elementary Classical Analysis',
    'QA',
    300,
    'M2868',
    '',
    '0716704528',
    'shelf'
);
INSERT INTO Books VALUES(
    412,
    'A Tour of the Calculus',
    'QA',
    303,
    'B488',
    '1995',
    '9780679426455',
    'shelf'
);
INSERT INTO Books VALUES(
    413,
    'A History of the Calculus and its Conceptual Development',
    'QA',
    303,
    'B79',
    '',
    '9780486605098',
    'shelf'
);
INSERT INTO Books VALUES(
    414,
    'Calculus',
    'QA',
    303,
    'C155',
    '1994',
    '9780471310556',
    'shelf'
);
INSERT INTO Books VALUES(
    415,
    'Calculus of a Single Variable',
    'QA',
    303,
    'D59',
    '1994',
    '9780534939366',
    'shelf'
);
INSERT INTO Books VALUES(
    416,
    'Calculus',
    'QA',
    303,
    'F27',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    417,
    'Calculus with Analytic Geometry: Alternative Fourth Addition',
    'QA',
    303,
    'L334',
    '1990',
    '0669178438',
    'shelf'
);
INSERT INTO Books VALUES(
    418,
    'What Is Calculus About?',
    'QA',
    303,
    'S28',
    '1961',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    419,
    'An Introduction to Analysis and Integration Theory',
    'QA',
    312,
    'P48',
    '1984',
    '9780486647470',
    'shelf'
);
INSERT INTO Books VALUES(
    420,
    'Precalculus',
    'QA',
    331.3,
    'C38',
    '1990',
    '9780395444641',
    'shelf'
);
INSERT INTO Books VALUES(
    421,
    'Precalculus: Enhanced with Graphing Utilities',
    'QA',
    331.3,
    'S926',
    '2009',
    '9780136015789',
    'shelf'
);
INSERT INTO Books VALUES(
    422,
    'Precalculus: Functions and Graphs',
    'QA',
    331.3,
    'S95',
    '1998',
    '0534377572',
    'shelf'
);
INSERT INTO Books VALUES(
    423,
    'Geometry for Today',
    'QA',
    445,
    'G48',
    '',
    '0669046434',
    'shelf'
);
INSERT INTO Books VALUES(
    424,
    'Elementary Geometry from an Advanced Standpoint',
    'QA',
    445,
    'M58',
    '1990',
    '0201508672',
    'shelf'
);
INSERT INTO Books VALUES(
    425,
    'Curves in Space',
    'QA',
    447,
    'J6',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    426,
    'Geometry',
    'QA',
    453,
    'J84',
    '1988',
    '0395430615',
    'shelf'
);
INSERT INTO Books VALUES(
    427,
    'Geometry',
    'QA',
    453,
    'J84',
    '1990',
    '0395461464',
    'shelf'
);
INSERT INTO Books VALUES(
    428,
    'Discovering Geometry',
    'QA',
    453,
    'S47',
    '1993',
    '0913684082',
    'shelf'
);
INSERT INTO Books VALUES(
    429,
    'Basic Geometry',
    'QA',
    455,
    'B552',
    '1999',
    '0821821016',
    'shelf'
);
INSERT INTO Books VALUES(
    430,
    'Elementary Geometry',
    'QA',
    455,
    'Z55',
    '1979',
    '0882758209',
    'shelf'
);
INSERT INTO Books VALUES(
    431,
    'Turtle Geometry',
    'QA',
    462,
    'A23',
    '1980',
    '0262010631',
    'shelf'
);
INSERT INTO Books VALUES(
    432,
    'A History of Pi',
    'QA',
    484,
    'B4',
    '1971',
    '0312381859',
    'shelf'
);
INSERT INTO Books VALUES(
    433,
    'Trigonometry with Applications',
    'QA',
    531,
    'G73',
    '1990',
    '0395461413',
    'shelf'
);
INSERT INTO Books VALUES(
    434,
    'Trigonometry',
    'QA',
    533,
    'D38',
    '',
    '0063825600',
    'shelf'
);
INSERT INTO Books VALUES(
    435,
    'Elementary Concepts of Topology',
    'QA',
    611,
    'A3813',
    '1961',
    '9780486607474',
    'shelf'
);
INSERT INTO Books VALUES(
    436,
    'Chaos, Fractals, and Dynamics: Computer Experiments in Mathematics',
    'QA',
    614.86,
    'D48',
    '1990',
    '9780201232882',
    'shelf'
);
INSERT INTO Books VALUES(
    437,
    'Fractals for the Classroom',
    'QA',
    614.86,
    'P45',
    '1991',
    '038797041X',
    'shelf'
);
INSERT INTO Books VALUES(
    438,
    'Differential Geometry of Curves and Surfaces',
    'QA',
    641,
    'C33',
    '',
    '0132125897',
    'shelf'
);
INSERT INTO Books VALUES(
    439,
    'Elementary Differential Geometry',
    'QA',
    641,
    'O5',
    '',
    '0125267509',
    'shelf'
);
INSERT INTO Books VALUES(
    440,
    'Lectures on Classical Differential Geometry',
    'QA',
    641,
    'S72',
    '1988',
    '9780486656090',
    'shelf'
);
INSERT INTO Books VALUES(
    441,
    'Basic Concepts of Geometry',
    'QA',
    681,
    'P7',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    442,
    'Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and the Unified Process',
    'QA',
    789,
    'O35 L37',
    '2001',
    '0130925691',
    'shelf'
);
INSERT INTO Books VALUES(
    443,
    'The Whole Shebang: A State-Of-The-Universe(S) Report',
    'QB',
    981,
    'F38',
    '1997',
    '9780684810201',
    'shelf'
);
INSERT INTO Books VALUES(
    444,
    'The Illustrated A Brief History of Time',
    'QB',
    981,
    'H377',
    '1998',
    '0553103741',
    'shelf'
);
INSERT INTO Books VALUES(
    445,
    'Cosmic Questions: Galactic Halos, Cold Dark Matter, and the End of Time',
    'QB',
    981,
    'M859',
    '1993',
    '9780471595212',
    'shelf'
);
INSERT INTO Books VALUES(
    446,
    'Theoretical Physics',
    'QC',
    20,
    'J81',
    '1986',
    '9780486652276',
    'shelf'
);
INSERT INTO Books VALUES(
    447,
    'The Physics of Baseball',
    'QC',
    26,
    'A23',
    '1994',
    '0060950471',
    'shelf'
);
INSERT INTO Books VALUES(
    448,
    'Physics for the Utterly Confused',
    'QC',
    30,
    'O66',
    '1999',
    '9780070482623',
    'shelf'
);
INSERT INTO Books VALUES(
    449,
    'AP Physics B and C',
    'QC',
    32,
    'J33',
    '2005',
    '9780071437134',
    'shelf'
);
INSERT INTO Books VALUES(
    450,
    'The Elegant Universe: Superstrings, Hidden Dimensions, and the Quest for the Ultimate Theory',
    'QC',
    794.6,
    'S85 G75',
    '1999',
    '9780965088800',
    'shelf'
);
INSERT INTO Books VALUES(
    451,
    'Earth',
    'QC',
    981.8,
    'C5 M3595',
    '2010',
    '9780764106767',
    'shelf'
);
INSERT INTO Books VALUES(
    452,
    'The Web of Life: A New Scientific Understanding of Living Systems',
    'QH',
    501,
    'C375',
    '1996',
    '9780385476751',
    'shelf'
);
INSERT INTO Books VALUES(
    453,
    'A Field Guide to Trees and Shrubs',
    'QK',
    110,
    'P47',
    '1972',
    '0395136512',
    'shelf'
);
INSERT INTO Books VALUES(
    454,
    'Guide to Mushrooms & Toadstools',
    'QK',
    617,
    'L364',
    '1963',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    455,
    'Complete Field Guide to American Wildlife: East, Central, and North',
    'QL',
    151,
    'C6',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    456,
    'Turtles of the Northeastern United States',
    'QL',
    666,
    'C5 B17',
    '',
    '0486205029',
    'shelf'
);
INSERT INTO Books VALUES(
    457,
    'A Field Book of North American Snakes',
    'QL',
    666,
    'O6 D615',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    458,
    'The Wonder of Birds',
    'QL',
    698.3,
    'W66',
    '1983',
    '0870444719',
    'shelf'
);
INSERT INTO Books VALUES(
    459,
    'Bonobo: The Forgotten Ape',
    'QL',
    737,
    'P96 W3',
    '1997',
    '9780520216518',
    'shelf'
);
INSERT INTO Books VALUES(
    460,
    'Apetalk and Whalespeak: The Quest for Interspecies Communication',
    'QL',
    776,
    'C7',
    '',
    '0874771803',
    'shelf'
);
INSERT INTO Books VALUES(
    461,
    'Light and Vision',
    'QP',
    475.5,
    'M8',
    '',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    462,
    'Songs of the Gorilla Nation',
    'RC',
    533,
    'A88 P756',
    '2004',
    '1400050588',
    'shelf'
);
INSERT INTO Books VALUES(
    463,
    'Numerical Methods and Software',
    'TA',
    345,
    'K34',
    '1989',
    '0136272584',
    'shelf'
);
INSERT INTO Books VALUES(
    464,
    'Networking Standards: A Guide to OSI, ISDN, LAN, and MAN Standards',
    'TK',
    5105.5,
    'S73',
    '1993',
    '9780201563573',
    'shelf'
);
INSERT INTO Books VALUES(
    465,
    'Home Networking Annoyances: How to Fix the Most Annoying Things About Your Home Network',
    'TK',
    5105.75,
    'I927',
    '2005',
    '9780596008086',
    'shelf'
);
INSERT INTO Books VALUES(
    466,
    'How the Internet Works',
    'TK',
    5105.875,
    'I57 G72423',
    '1999',
    '9780789721327',
    'shelf'
);
INSERT INTO Books VALUES(
    467,
    'Where Wizards Stay Up Late: The Origins of the Internet',
    'TK',
    5105.875,
    'I57 H338',
    '1996',
    '9780684812014',
    'shelf'
);
INSERT INTO Books VALUES(
    468,
    'Computer Networking: A Top-Down Approach Featuring the Internet',
    'TK',
    5105.875,
    'I57 K876',
    '2001',
    '9780201477115',
    'shelf'
);
INSERT INTO Books VALUES(
    469,
    'Weaving the Web: The Original Design and Ultimate Destiny of the World Wide Web by Its Inventor',
    'TK',
    5105.888,
    'B46',
    '1999',
    '9780062515872',
    'shelf'
);
INSERT INTO Books VALUES(
    470,
    'Web Design on a Shoestring',
    'TK',
    5105.888,
    'B515',
    '2003',
    '9780735713284',
    'shelf'
);
INSERT INTO Books VALUES(
    471,
    'CSS for Web Designers',
    'TK',
    5105.888,
    'C434',
    '2010',
    '9780984442522',
    'shelf'
);
INSERT INTO Books VALUES(
    472,
    'The Zen of CSS Design: Visual Enlightenment for the Web',
    'TK',
    5105.888,
    'S477',
    '2005',
    '9780321303479',
    'shelf'
);
INSERT INTO Books VALUES(
    473,
    'Designing with Web Standards',
    'TK',
    5105.888,
    'Z46',
    '2003',
    '9780735712010',
    'shelf'
);
INSERT INTO Books VALUES(
    474,
    'Zope 3 Developer''s Handbook',
    'TK',
    5105.8885,
    'Z65 R53',
    '2005',
    '0672326175',
    'shelf'
);
INSERT INTO Books VALUES(
    475,
    'Web Component Development with Zope 3',
    'TK',
    5105.8885,
    'Z65 W45',
    '2007',
    '9783540338079',
    'shelf'
);
INSERT INTO Books VALUES(
    476,
    'Manual of Airborne Topographic Lidar',
    'TK',
    6592,
    'O6 M36',
    '2012',
    '9781570830976',
    'shelf'
);
INSERT INTO Books VALUES(
    477,
    'Basic Electronics',
    'TK',
    7815,
    'U5134',
    '1973',
    '9780486210766',
    'shelf'
);
INSERT INTO Books VALUES(
    478,
    'TTL Cookbook',
    'TK',
    7868,
    'L6 L36',
    '',
    '0672210355',
    'shelf'
);
INSERT INTO Books VALUES(
    479,
    'The Conquest of the Microchip',
    'TK',
    7874,
    'Q3613',
    '1988X',
    '9780674162976',
    'shelf'
);
INSERT INTO Books VALUES(
    480,
    'The Elements of Computing Systems: Building a Modern Computer from First Principles',
    'TK',
    7888.3,
    'N57',
    '2005eb',
    '9780262640688',
    'shelf'
);
INSERT INTO Books VALUES(
    481,
    'Getting Started with Beaglebone',
    'TK',
    7895,
    'E42 R43',
    '2013',
    '9781449345372',
    'shelf'
);
INSERT INTO Books VALUES(
    482,
    'Darkroom',
    'TR',
    148,
    'D28',
    '',
    '0912810203',
    'shelf'
);
INSERT INTO Books VALUES(
    483,
    'On Photography',
    'TR',
    183,
    'S65',
    '1977',
    'N/A',
    'shelf'
);
INSERT INTO Books VALUES(
    484,
    'The Keepers of the Light: A History and Working Guide to Early Photographic Processes',
    'TR',
    330,
    'C68',
    '',
    '0871001586',
    'shelf'
);
INSERT INTO Books VALUES(
    485,
    'Camera Work: A Pictorial Guide',
    'TR',
    653,
    'C34',
    '1978',
    '0486235912',
    'shelf'
);
INSERT INTO Books VALUES(
    486,
    'The Portfolios of Ansel Adams',
    'TR',
    654,
    'A34',
    '',
    '0821207237',
    'shelf'
);
INSERT INTO Books VALUES(
    487,
    'Russell Lee, Photographer',
    'TR',
    820.5,
    'L42',
    '',
    '0871001519',
    'shelf'
);
INSERT INTO Books VALUES(
    488,
    'The Wikipedia Revolution: How a Bunch of Nobodies Created the World''s Greatest Encyclopedia',
    'ZA',
    4482,
    'L54',
    '2009',
    '9781401303716',
    'shelf'
);
