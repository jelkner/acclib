#!/usr/bin/env python3
import bottle


@bottle.get('/')
def index():
    return bottle.template('views/welcome')


bottle.debug(True)
bottle.run(host='0.0.0.0', port=8090)
