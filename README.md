# ACC IT/CSC Library 

A Python program to maintain a list of books for the Arlington Career Center
Information Technology / Computer Science Library 

## LOC Resource Links

* [Structure of the LC Control Number](https://www.loc.gov/marc/lccn_structure.html)
* [Library of Congress Call Numbers: A Guide for Non-Catalogers Who Suddenly
   Find Themselves with a Cataloging Job](https://www.slideshare.net/akroeger/kroeger-lcclassification)
* [The Cataloging Calculator](http://calculate.alptown.com)
* [Library of Congress Classification Guide](http://home.olemiss.edu/~tharry/LC/lccguide.pdf)
* [Library of Congress Classification (LCC) History and Development](https://www.librarianshipstudies.com/2017/11/library-of-congress-classification-history.html)
* [Understanding LC Call Numbers](https://library.uaf.edu/ls101-call-numbers)
* [Library of Congress Classification System (YouTube)](https://www.youtube.com/watch?v=Vdh3O5PdEiw)
* [LOC Call Numbers (YouTube)](https://www.youtube.com/watch?v=fMUO6RGmL8w)
* [How to read a Library of Congress (LC) Call Number](https://www.angelo.edu/services/library/handouts/lcnumber.php)
* [Cutter Numbers](https://www.loc.gov/aba/publications/FreeCSM/G063.pdf)
* [Library of Congress Classification and Shelflisting Manual](https://www.loc.gov/aba/publications/FreeCSM/freecsm.html)
* [Book Numbers](https://www.miskatonic.org/library/book-numbers.html)
* [Library of Congress Classification Guide](http://home.olemiss.edu/~tharry/LC/lccguide.pdf)
* [How to generate a cutter number for a book](https://anjackson.github.io/zombse/062013%20Libraries%20&%20Information%20Science/static/questions/1034.html)
