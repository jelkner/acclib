Test Scanning ISBN Numbers
==========================

Adding books to our library begins with scanning the ISBN number of the new
books and adding that to a data file.

Import functions to test scanning ISBN numbers.

  >>> from acclib import scan_to_isbn, read_isbn_dat

Setup test data file and test_input function that reads ISBN numbers from a
generator ending with a 'Q' to quit. 

  >>> import io
  >>> f = io.StringIO('9781782161523\n9780201403756\n9781568842035')
  >>> isbn_list = read_isbn_dat(f)
  >>> print(isbn_list)
  ['9781782161523', '9780201403756', '9781568842035']
  >>> def generate_sample_input():
  ...     for isbn in '9780804843676', '9780763652791', '9780130619898',\
  ...                 '9781782161523', '9780133708752', 'Q':
  ...         yield isbn
  >>> isbn_gen = generate_sample_input() 
  >>> def test_input(prompt='', gen=isbn_gen):
  ...     return next(gen)

Now call scan_to_isbn with the test input data.

  >>> isbn_list_out = scan_to_isbn(isbn_list, test_input)
  ISBN number 9780804843676 added to list.
  ISBN number 9780763652791 added to list.
  9780130619898 is not a valid ISBN number.
  ISBN number 9781782161523 was already in list.
  ISBN number 9780133708752 added to list.
  >>> len(isbn_list_out)
  6
  >>> '9780133708752' in isbn_list_out
  True
  >>> isbn_list.count('9781782161523')
  1


Test Inserting New Books into Book List File
============================================

Create two "files" for testing insert_new_books.

  >>> books_csv = """TITLE;CLASS;SUBCLASS;CUTTER;SUPPL;ISBN;STATUS
  ... Book 0;DT;1974;S26;1999;9780375400193;N
  ... Book 1;GF;41;H93;2012;9780078021466;N
  ... Book 2;HM;851;O973;2016;9781944869335;N
  ... Book 3;LC;149;F76;1985;9780897890434;N
  ... Book 4;LC;4091;K69;1991;9780517582213;N
  ... Book 5;Q;336;B74;2001;9780201403756;N
  ... Book 6;QA;76.6;K495;1984;013937681X;N
  ... Book 7;QA;76.73;C15K47;1988;9780131103702;N
  ... Book 8;QA;76.73;C28G69;1996;9780133708752;N
  ... Book 9;QA;76.73;J38E25;1998;9780136597230;N"""
  >>> newbooks_csv = """TITLE;CLASS;SUBCLASS;CUTTER;SUPPL;ISBN;STATUS
  ... Book J;PZ;10.831;Y29N47;2012;9780763652791;Y
  ... Book I;BP;223;Z8L57636;2011;9780670022205;Y
  ... Book H;QC;20;J81;1986;9780486652276;Y
  ... Book G;QA;76.76;O63U54518;1994;9781568842035;Y
  ... Book F;HD;58.9;W526;2015;9781625275271;Y
  ... Book E;QA;76.73;P98M42;2013;9781449319793;Y
  ... Book D;DB;855;M67;1989;9780684191430;Y
  ... Book C;G;70.212;O23;2015;9781617291395;Y
  ... Book B;G;70.212;M287;2010;9781420087338;Y
  ... Book A;PL;1129;E5W723;2012;9780804843676;Y"""

Create an in-memory file object with the csv book "files".

  >>> fb = io.StringIO(books_csv)
  >>> fnb = io.StringIO(newbooks_csv)

Create a csv reader and call read_books to generate a list of book objects.
There should be 10 of them.

  >>> import csv 
  >>> from acclib import read_books
  >>> bookreader = csv.reader(fb, delimiter=';', quotechar='|')
  >>> books = read_books(bookreader)
  >>> len(books)
  10

Read in the new books the same way.

  >>> bookreader = csv.reader(fnb, delimiter=';', quotechar='|')
  >>> newbooks = read_books(bookreader)
  >>> len(books)
  10

Pop off a new book from the end of the list and find where it fits in the book
list.  Since the new book has call number PL1129 .E5 W723 2012, it should go
between LC4091 .K69 1991 and Q336 .B74 2001, at position 5.

  >>> from acclib import find_book_insert_pos
  >>> book = newbooks.pop()
  >>> callnum = book.callnum()
  >>> callnum_list = [book.callnum() for book in books]
  >>> find_book_insert_pos(callnum, callnum_list)
  5

Insert the new book in books and use Python's sorted to confirm it is in its
correct spot.

  >>> from acclib import insert_book_in_booklist 
  >>> books = insert_book_in_booklist(book, books)
  >>> len(books) 
  11
  >>> books == sorted(books, key=lambda book: book.callnum())
  True

Try again with a new book that should go between books 0 and 1.

  >>> book = newbooks.pop()
  >>> callnum = book.callnum()
  >>> callnum_list = [book.callnum() for book in books]
  >>> find_book_insert_pos(callnum, callnum_list)
  1
  >>> books = insert_book_in_booklist(book, books)
  >>> len(books) 
  12
  >>> books == sorted(books, key=lambda book: book.callnum())
  True

Now pop off the rest of the new books and insert each in turn.

  >>> while newbooks:
  ...     books = insert_book_in_booklist(newbooks.pop(), books)
  >>> len(newbooks)
  0
  >>> len(books)
  20

And confirm we have the same order sorted would give us.

  >>> books == sorted(books, key=lambda book: book.callnum())
  True

Write sorted books to books file.

  >>> from acclib import write_books
