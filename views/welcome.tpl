%include('top.tpl', subtitle='Welcome to the ACC IT/CSC Library!')
<p>
This website is both a web application project created by web application
development students at the Arlington Career Center and a community service
project aiming to make books from our IT/CSC library available to folks who
want to use them.
</p>
%include('bottom.tpl')
