<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>ACC IT/CS Library Book List</title>
<style>
body {
    margin: 4vw;
}
main {
    margin: 4vw;
    border: 1px dotted #555;
}
h1 {
    text-align: center;
}
table {
    margin-left: auto;
    margin-right: auto;
}
tr.new {
    color: #822;
}
@media print {
    body {
        margin: 0;
        color: black;
        size: portrait;
    }
    main {
        margin: auto;
        font-size: 80%;
        border: none;
    }
    @page {
        margin: 0;
    }
}
@page {
    size: 8.5in 11in;
}
</style>
</head>
<body>
<main>
<h1>ACC IT/CS Library Book List</h1>
<table>
<tr>
<th>Title</th>
<th>LOC Call Number</th>
<th>ISBN</th>
</tr>
