<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>ACC IT/CSC Library</title>
<style type="text/css">
body {
    display: grid;
}
footer {
    text-align: center;
}
</style>
</head>
<body>
<header>
<h1>ACC IT/CSC Library</h1>
% if defined('subtitle'):
<h2>{{subtitle}}</h2>
% end
</header>

<nav>
</nav>

<main>
