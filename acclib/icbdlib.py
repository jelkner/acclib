import csv
import os
import random
import string
import subprocess as sp
from multiprocessing.pool import ThreadPool

import pymarc
from pyaz import zoom

# Connect to MARC21 databases using the Z39.50 protocol

# Define constant variables

# Predefined URLs, Ports, and Database Names for servers
DATABASES = [
    ('z3950.loc.gov', 7090, 'voyager',),
    ('lx2.loc.gov', 210, 'LCDB',),
    ('library.mit.edu', 9909, 'mit01pub',),
    ('albert.rit.edu', 210, 'innopac',),
    ('vmvm.alc.org', 2200, 'public'),
    ('catalog.lib.utexas.edu', 210, 'innopac'),
]

# Preferred Record Syntax
PREF_REC_SYNTAX = 'USMARC'


# Query Type
QUERY_TYPE = 'PQF'


# Establish a connection to a single database
def __connect_to_database(database):
    try:
        return zoom.Connection(
            database[0],
            database[1],
            databaseName=database[2],
            timeout=5,
            sru=None,
            extraArgs=''
        )
    except (ConnectionError, TimeoutError) as e:
        return None


# Connect to the databases defined in config.py
def connect_to_databases():
    pool = ThreadPool(len(DATABASES))
    return list(pool.map(__connect_to_database, DATABASES))


# Disconnect from the databases
def disconnect_from_databases(databases):
    for database in databases:
        del database


# Definitions for exceptions and warnings
class InvalidISBN(Exception):
    """
    Exception raised when the ISBN is invalid according to
    the validation algorithm
    """


class NoDatabases(Exception):
    """
    Exception raised when there are no databases to gather
    data from (connecting failed)
    """


class IncompleteData(Warning):
    """
    Warning raised when the book data is incomplete
    """


# Define the Book class that is the root of the library

# The Book class gathers and stores book data
class IcbdBook:

    # Initialize class instance
    def __init__(self, isbn, databases):
        self.title = ""
        self.lcc = ""
        self.isbn = str(isbn)
        if not self.__is_isbn_valid():
            raise InvalidISBN
        self.databases = databases
        self.__gather()
        self.__format_title()
        self.__format_lcc()

    # to_list used to retrieve data stored in class instance
    def to_list(self):
        return [self.title, self.lcc, self.isbn]

    # __repr__ used to retrieve string with data stored in class instance
    def __repr__(self):
        return f"Book(title={self.title}, lcc={self.lcc}, isbn={self.isbn})"

    # Gather book data from MARC21 servers
    def __gather(self):
        random.shuffle(self.databases)
        if self.databases is None:
            raise NoDatabases
        for database in self.databases:
            if database is None:
                continue
            query = zoom.Query(QUERY_TYPE, f'@attr 1=7 {self.isbn}')
            try:
                result = database.search(
                             query,
                             preferredRecordSyntax=PREF_REC_SYNTAX
                         )
            except Exception as e:
                continue
            if len(result) > 0:
                try:
                    reader = list(pymarc.MARCReader(
                        list(result)[0].get('raw')))
                except Exception as e:
                    continue
                for record in reader:
                    if self.title == '' and record.title() is not None:
                        self.title = record.title()
                    if self.lcc == '':
                        try:
                            self.lcc = record.get_fields('050')[0].value()
                        except IndexError:
                            pass
            if self.title != '' and self.lcc != '':
                return
        raise IncompleteData

    # Format Title
    def __format_title(self):
        if self.title.endswith('.'):
            self.title = self.title[:-1]
        if self.title.endswith('\\'):
            self.title = self.title[:-1]
        if self.title.endswith('-'):
            self.title = self.title[:-1]
        if self.title.endswith(' '):
            self.title = self.title[:-1]
        if self.title.endswith(' /'):
            self.title = self.title[:-2]
        if self.title.endswith(' //r'):
            self.title = self.title[:-4]
        if self.title.endswith(','):
            self.title = self.title[:-1]

    # Format LCC
    def __format_lcc(self):
        if self.lcc.endswith('.'):
            self.lcc = self.lcc[:-1]
        if self.lcc.endswith('\\'):
            self.lcc = self.lcc[:-1]
        if self.lcc.endswith('-'):
            self.lcc = self.lcc[:-1]
        if self.lcc.endswith(' '):
            self.lcc = self.lcc[:-1]
        if self.lcc.endswith(' /'):
            self.lcc = self.lcc[:-2]
        if self.lcc.endswith(' //r'):
            self.lcc = self.lcc[:-4]
        if self.lcc.endswith(','):
            self.lcc = self.lcc[:-1]
        # self.lcc = self.lcc.replace(" ", "")

    # Check if ISBN is valid
    def __is_isbn_valid(self):
        if len(self.isbn) == 13:
            if not self.isbn.isdigit():
                return False
            checksum = 0
            one_three = 1
            for digit in self.isbn:
                checksum += one_three * int(digit)
                one_three = 1 if one_three == 3 else 3
            return checksum % 10 == 0
        if len(self.isbn) == 10:
            if not self.isbn.isdigit():
                if not self.isbn[-1].isdigit():
                    return False
            checksum = 0
            for offset, value in enumerate(self.isbn):
                if value == 'X' or value == 'x':
                    checksum += (10 - offset) * int(10)
                else:
                    checksum += (10 - offset) * int(value)
            return checksum % 11 == 0
        return False


if __name__ == '__main__':
    import doctest
    doctest.testmod()
