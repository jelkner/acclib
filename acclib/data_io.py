import csv
import os
import string

from .books import Book, is_valid_isbn
from .icbdlib import IcbdBook


# Tools for scanning ISBN numbers from books and creating book list

def read_isbn_dat(fp):
    raw_list = fp.readlines()
    return [isbn.strip() for isbn in raw_list]


def scan_to_isbn(isbn_list, input_func=input):
    """
    Scan ISBN numbers from books into a list of ISBN numbers.
    """
    isbns = isbn_list[:]

    while True:
        isbn = input_func('Scan ISBN (or type "Q" to quit): ')
        if isbn == 'Q':
            break
        if not is_valid_isbn(isbn):
            print(f'{isbn} is not a valid ISBN number.')
            continue
        if isbn not in isbn_list:
            isbns.append(isbn)
            print(f'ISBN number {isbn} added to list.')
        else:
            print(f'ISBN number {isbn} was already in list.')

    return isbns


def isbn_to_book(isbn, databases):
    """
    Lookup book from ISBN number.
    """
    book = IcbdBook(isbn, databases)
    return Book(book.title, book.lcc, isbn, 'Y')


def read_books(bookreader):
    """Read list of books from csv file into list of Book objects"""
    booklist = []
    # skip heading line
    next(bookreader)
    for rawbook in bookreader:
        booklist.append(Book(*(rawbook)))

    return booklist


def write_books(booklist, bookfp):
    """Write list of Book objects to a csv file"""
    # Write header line
    bookfp.write('TITLE;CLASS;SUBCLASS;CUTTER;SUPPL;ISBN;STATUS')

    # Write each book on its own line
    for book in booklist:
        bookfp.write(
            f'{book.title};{book.title};{book.callnum.to_csv()};{book.isbn}'
        )
