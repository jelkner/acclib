import string


# Functions to handle ISBN and parse LC call numbers.
#
# Start with ISBN
def is_valid_isbn(isbn):
    """
    Check for valid 10 digit and 13 digit ISBN number.

      >>> is_valid_isbn('0131103628')
      True
      >>> is_valid_isbn('0131103629')
      False
      >>> is_valid_isbn('978-1593279509')
      True
      >>> is_valid_isbn('013937681X')
      True
      >>> is_valid_isbn('0201882590')
      True
      >>> is_valid_isbn('N/A')
      True
    """
    # handle special case for old books before 1967 that do not have one
    if isbn == 'N/A':
        return True
    # remove dashes before checking
    isbn = isbn.replace('-', '')
    if len(isbn) not in (10, 13):
        return False
    if len(isbn) == 10:
        # Compute sum of right-to-left position times digits.
        weighted_sum = 0
        # Multiply all but the last digit by its position
        for i in range(9):
            weighted_sum += int(isbn[i]) * (10 - i)
        # Check for X in last digit
        weighted_sum += 10 if isbn[9] == 'X' else int(isbn[9])
        # Check if weighted_sum modulo 11 is 0
        return weighted_sum % 11 == 0

    # len(isbn) == 13
    m = 1
    weighted_sum = 0
    for d in isbn:
        weighted_sum += m * int(d)
        m = 3 if m == 1 else 1
    # Check if weighted_sum modulo 10 is 0
    return weighted_sum % 10 == 0


def isbn10to13(isbn):
    """
    Convert a 10 digit ISBN to a 13 digit ISBN using algorithm found here:
    https://isbn-information.com/convert-isbn-10-to-isbn-13.html

      >>> isbn10to13('0684812010')
      '9780684812014'
      >>> isbn10to13('0201612941')
      '9780201612943'
      >>> isbn10to13('0716782715')
      '9780716782711'
      >>> isbn10to13('013937681X')
      '9780139376818'
    """
    digits = '978' + isbn[:-1]
    digits_sum = sum([int(d) * 3 ** (i % 2) for i, d in enumerate(digits)])
    checksum = 10 - digits_sum % 10
    return digits + str(checksum)


# LC call numbers

def end_of_subclass(pos, subclsstr):
    """
    Returns True if pos marks the end of an LC call number subclass value

      >>> end_of_subclass(0, '76.73.J39')
      False
      >>> end_of_subclass(2, '76.73.J39')
      False
      >>> end_of_subclass(5, '76.73.J39')
      True
      >>> end_of_subclass(2, '70.212 .W47 2013')
      False
    """
    assert(pos < len(subclsstr))

    if subclsstr[pos].isdigit():
        return False
    if subclsstr[pos] == '.' and subclsstr[pos+1].isdigit():
        return False
    return True


def find_cutter(s, start=0):
    """
    Extract a cutter number from part of an LC call number string, and return
    a tuple with it and the index where it begins.  Cutter numbers are
    characterized by an upper case letter followed by a sequence of digits.
    They are often (but not always) proceeded by a '.'

      >>> find_cutter('.J39 H38 2019')
      ('J39', 1)
      >>> find_cutter('.J39 H38 2019', 2)
      ('H38', 5)
      >>> find_cutter(' .I5858 2001')
      ('I5858', 2)
      >>> find_cutter('')
      ('', 0)
      >>> find_cutter('2001')
      ('', 0)
      >>> find_cutter('v.1')
      ('', 0)
      >>> find_cutter(' 1995 v.1', 3)
      ('', 3)
      >>> find_cutter('D')
      ('', 0)
    """
    pos = start

    while pos < len(s) and s[pos] not in string.ascii_letters:
        pos += 1

    # Handle reached end of string and cutter not found
    if pos > len(s)-2:
        return ('', start)

    if s[pos] in string.ascii_letters and s[pos+1].isdigit():
        # Mark start of cutter
        first = pos
        pos += 1
        # Continue until last digit
        while pos < len(s) and s[pos].isdigit():
            pos += 1

        return (s[first:pos], first)

    # Handle volume and copy numbers
    return ('', start)


def make_callnum_tuple(cnstr):
    """
    Split a call number string into a tuple with the following elements:

        1. Alpha class[subclass]
        2. Numeric subclass
        3. Cutter number(s) with .'s removed
        4. The rest (year, volume, etc)

      >>> make_callnum_tuple('QA76.73.J39 H38 2019')
      ('QA', 76.73, 'J39H38', '2019')
      >>> make_callnum_tuple('QA76.73.C15 K47 1988')
      ('QA', 76.73, 'C15K47', '1988')
      >>> make_callnum_tuple('LB2323.C55 2008')
      ('LB', 2323, 'C55', '2008')
      >>> make_callnum_tuple('LB880.F732 P432 1994')
      ('LB', 880, 'F732P432', '1994')
      >>> make_callnum_tuple('G70.212 .W47 2013')
      ('G', 70.212, 'W47', '2013')
      >>> make_callnum_tuple('Q336 .B74 2001')
      ('Q', 336, 'B74', '2001')
      >>> make_callnum_tuple('PS3204 1992')
      ('PS', 3204, '', '1992')
    """
    # Extract class letters into clss, leaving rest in cns
    #
    # First make a copy to mangle
    cns = cnstr
    # Start with empty cutter and rest to handle edge cases where they
    # don't exist
    cutter = ''
    rest = ''

    # Extract the class by finding where it ends
    end = 0
    while cns[end] in string.ascii_letters:
        end += 1

    # Do a quick sanity check that the next value is a digit
    assert(cns[end].isdigit())

    # Use slice to separate class from rest
    clss = cns[:end]
    cns = cns[end:]

    # Extract the subclass by finding where it ends
    end = 0
    # Look for . followed by letter, space, or end of call num
    while end < len(cns) and not end_of_subclass(end, cns):
        end += 1
    # with end found, seperate subclass from rest
    subclss = cns[:end]
    cns = cns[end:]
    # strip off space if it exists
    cns = cns.strip()

    # convert subclass to int or float or raise ValueError
    if '.' in subclss:
        try:
            subclss = float(subclss)
        except ValueError:
            print(f'{subclss} in {cnstr} is not a decimal number.')
    else:
        try:
            subclss = int(subclss)
        except ValueError:
            print(f'{subclss} in {cnstr} is not a whole number.')

    # Extract cutter number(s) if there are any
    acutter, pos = find_cutter(cns)
    cutter_nums = []

    while acutter:
        cutter_nums.append(acutter)
        acutter, pos = find_cutter(cns, pos+len(acutter))

    # If cutters were found, joint them into a string and return rest of cns
    if cutter_nums:
        cutter = ''.join(cutter_nums)

    suppl = cns[pos:].strip()

    return (clss, subclss, cutter, suppl)


# CallNumber and Book classes
class CallNumber:
    """
    Defines a comparable Library of Congress call number

      >>> cn1 = CallNumber('QA76.73.J39 H38 2019')
      >>> print(cn1)
      QA76.73 .J39H38 2019
      >>> cn2 = CallNumber('LB880.F732 P432 1994')
      >>> cn1 > cn2
      True
      >>> cn3 = CallNumber('LB1025.2 .F73 1987')
      >>> print(cn3)
      LB1025.2 .F73 1987
      >>> cn2 < cn3
      True
      >>> cn4 = CallNumber('G70.212 .W47 2013')
      >>> cn5 = CallNumber('GF41 .H93 2012')
      >>> cn4 < cn5
      True
      >>> cn6 = CallNumber('Q336 .B74 2001')
      >>> cn7 = CallNumber('QA76.6 .K495 1984')
      >>> cn6 < cn7
      True
      >>> cn8 = CallNumber('PS3204 1992')
      >>> print(cn8)
      PS3204 1992
      >>> cn9 = CallNumber(('QA', 76.73, 'J39H38', '2019'))
      >>> print(cn9)
      QA76.73 .J39H38 2019

    Print a call numbers as a ; delineated string for direct sqlite import

      >>> cn1.csv()
      'QA;76.73;J39H38;2019'
      >>> cn2.csv()
      'LB;880;F732P432;1994'
      >>> cn8.csv()
      'PS;3204;;1992'
    """
    def __init__(self, cn):
        self.callnum = cn if isinstance(cn, tuple) else make_callnum_tuple(cn)

    def __eq__(self, other):
        if len(self.callnum) != len(other.callnum):
            return False
        for i in range(len(self.callnum)):
            if self.callnum[i] != other.callnum[i]:
                return False
        return True

    def __ne__(self, other):
        return not(self == other)

    def __gt__(self, other):
        if self.callnum[0] > other.callnum[0]:
            return True
        if self.callnum[0] < other.callnum[0]:
            return False
        if self.callnum[1] > other.callnum[1]:
            return True
        if self.callnum[1] < other.callnum[1]:
            return False
        if self.callnum[2] > other.callnum[2]:
            return True
        if self.callnum[2] < other.callnum[2]:
            return False
        return False

    def __lt__(self, other):
        return other > self

    def __le__(self, other):
        return self < other or self == other

    def __ge__(self, other):
        return self > other or self == other

    def __repr__(self):
        s = f'{self.callnum[0]}{self.callnum[1]}'
        if self.callnum[2]:
            s += f' .{self.callnum[2]}'
        if self.callnum[3]:
            s += f' {self.callnum[3]}'
        return s

    def csv(self):
        return f'{self.callnum[0]};{self.callnum[1]};{self.callnum[2]};'\
               f'{self.callnum[3]}'


class Book:
    """
    A comparable book object ordered by Library of Congress call number.

      >>> b = Book('The Wealth of Nations', 'HB', 161, 'S65',
      ...          '2003', '9780553585971', 'S')
      >>> b.callnum()
      'HB161 .S65 2003'
      >>> b.csv()
      'The Wealth of Nations;HB;161;S65;2003;9780553585971;S'
    """
    def __init__(self, title, clss, subclss, cutter, suppl, isbn, status='S'):
        self.title = title
        self.clss = clss
        self.subclss = float(subclss) if '.' in str(subclss) else int(subclss)
        self.cutter = cutter
        self.suppl = suppl
        self.status = status

        isbn = isbn.replace('-', '')
        if not is_valid_isbn(isbn):
            raise ValueError(f'Invalid ISBN number: {isbn}')
        else:
            self.isbn = isbn if len(isbn) == 13 else isbn10to13(isbn)

    def callnum(self):
        return f'{self.clss}{self.subclss} .{self.cutter} {self.suppl}'

    def __str__(self):
        return f'{self.callnum()}\t{self.title}'

    def __repr__(self):
        return f'({self.title}, {self.callnum()}, {self.isbn})'

    def csv(self):
        return f'{self.title};{self.clss};{self.subclss};{self.cutter};'\
               f'{self.suppl};{self.isbn};{self.status}'


# Functions for sorting and manipulating csv book lists

def find_book_insert_pos(callnum, callnums):
    """
    Determine where in a sorted list of books a new book belongs.

      >>> cns = [
      ...     CallNumber('DT1974 .S26 1999'), CallNumber('GF41 .H93 2012'),
      ...     CallNumber('HM851 .O973 2016'), CallNumber('LC149 .F76 1985'),
      ...     CallNumber('LC4091 .K69 1991'), CallNumber('Q336 .B74 2001')
      ... ]
      >>> cn1 = CallNumber('G70.212 .O23 2015')
      >>> find_book_insert_pos(cn1, cns)
      1
      >>> cn2 = CallNumber('PZ10.831 .Y29 N47 2012')
      >>> find_book_insert_pos(cn2, cns)
      5
      >>> cn3 = CallNumber('BP223 .Z8 L57636 2011')
      >>> find_book_insert_pos(cn3, cns)
      0
      >>> cn4 = CallNumber('QA76.73 .P98 M42 2013')
      >>> find_book_insert_pos(cn4, cns)
      6
    """
    # Do a binary search to find location for new book
    low, high = 0, len(callnums) - 1

    # Handle cases where new book goes at beginning or end of list
    if callnum < callnums[0]:
        return 0
    if callnum > callnums[-1]:
        return len(callnums)

    # Handle cases where new book is inserted within booklist
    while low < high:
        mid = (low + high) // 2
        if callnum < callnums[mid]:
            high = mid
        else:
            low = mid + 1

    return low


def insert_book_in_booklist(book, booklist):
    """
    Return a new booklist with book inserted in proper position.
    """
    pos = find_book_insert_pos(
              book.callnum(),
              [book.callnum() for book in booklist]
          )
    return booklist[:pos] + [book] + booklist[pos:]


def sort_books(booklist):
    """Return a sorted list of Book objects"""
    return sorted(booklist, key=lambda book: book.callnum)


def format_book_title(raw_title):
    """
    Take a raw book title as returned by icbd lookup and format it.

      >>> sin = 'Object persistence : beyond object-oriented databases'
      >>> sout = format_book_title(sin)
      >>> sout
      'Object Persistence: Beyond Object-Oriented Databases'
      >>> sin = 'Karel++ : a gentle introduction to the art of '
      >>> sin += ' object-orinted programming'
      >>> sout = format_book_title(sin)
      >>> sout
      'Karel++: A Gentle Introduction to the Art of Object-Orinted Programming'
      >>> sin = 'Algorithms to live by : The computer science of human '
      >>> sin += 'decisions'
      >>> sout = format_book_title(sin)
      >>> sout
      'Algorithms to Live By: The Computer Science of Human Decisions'
    """
    # Create list of words not to capitalize
    nocaps = ['at', 'by', 'down', 'for', 'from', 'in', 'into', 'like', 'near']
    nocaps += ['of', 'off', 'on', 'onto', 'over', 'past', 'to', 'upon']
    nocaps += ['with', 'and', 'as', 'but', 'for', 'if', 'nor', 'once']
    nocaps += ['or', 'so', 'than', 'that',  'till', 'when', 'yet', 'a', 'the']

    # Remove spaces in front of colons
    raw_title = raw_title.replace(' :', ':')

    # Split the title into words on spaces
    words = raw_title.split()

    for pos, word in enumerate(words):
        # Capitalize words not in nocaps list unless after colon
        if word not in nocaps or ':' in words[pos] or ':' in words[pos-1]:
            words[pos] = words[pos].capitalize()
        # Capitalize words after hypen
        if '-' in word:
            words[pos] = words[pos].title()

    return ' '.join(words)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
