# ACC Library Tools 

Store and organize a collection of books using Library of Congress (LC) 
call numbers for ordering and ISBN numbers for unique identification.

## Dependencies

Run the following to install dependencies on a debian / ubuntu system:

```
$ sudo apt install libyaz5
$ pipenv install pymarc
$ pipenv install git+https://gitlab.com/mjsir911/pyaz.git#egg=pyaz
```

## Files

* books.py - Class for books with comparable 
* scan.py - Scan ISBN numbers on books and lookup book titles and Library of
  Congress (LC) call numbers.


## Resource Links

* [How to Verify an ISBN](https://www.instructables.com/id/How-to-verify-a-ISBN/)
